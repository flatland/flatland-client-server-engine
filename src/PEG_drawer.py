from PEG_constants import *
from PEG_datatypes import *
import PEG_mainLoop
import os
import pygame
import PEG_sound
import pedo_lookup

#temp stuff for testing
pygame.init()
screen = pygame.display.set_mode(SCREEN_SIZE,pygame.FULLSCREEN|pygame.DOUBLEBUF)   


#maps names to surfaces with images loaded
imr = dict()
imr['tempshape'] = pygame.image.load(os.path.join('data','sixspritescaled.png')).convert()
#imr['tempshape'].set_colorkey(Color)
imr['polygons'] = pygame.image.load(os.path.join('data2','polys.png')).convert()
imr['polygons'].set_colorkey((0,255,0))
imr['+1'] = pygame.image.load(os.path.join("data2","plus_one.png"))
imr['+1'].set_colorkey((255,255,255))

#maps names to their respective linear animations
linanim = dict()
linanim['tempshape'] = [pygame.Rect(0,0,80,80),pygame.Rect(80,0,80,80)]
linanim['+1'] = [pygame.Rect(0,0,800,480),pygame.Rect(0,480,800,480),pygame.Rect(0,480*2,800,480)]

def drawImgRefAt(img,location):
    """
    blits imr[img] at position location (vector2d) blits entire image
    """
    #cam = PEG_mainLoop.mainLoop().cam
    #screen = PEG_mainLoop.mainLoop().screen
    #TODO add camera shake stuff
    global imr,screen
    screen.blit(imr[img],location.getIntTuple())
def drawAnim(anim,location,frame=0):
    pass
def drawAnimCentered(anim,frame=0):
    global imr,screen,linanim
    box = linanim[anim][frame]
    #we bypass the camera here
    screen.blit(imr[anim],((screen.get_width()-box.width)/2 ,(screen.get_height()-box.height)/2 ),box)
def drawShapeEn(color,sides,resources,location,frame = 0):
    #cam = PEG_mainLoop.mainLoop().cam
    #screen = PEG_mainLoop.mainLoop().screen
    global imr,screen,linanim
    ypos = pedo_lookup.positionMap[color]
    xpos = sides - 3
    #draw the polygon
    screen.blit(imr['polygons'],location.getIntTuple(),pygame.Rect(xpos*30,ypos*30,30,30))
    #TODO draw resources
def rotationtest(color,sides,resources,location,rotation,frame = 0):
    #cam = PEG_mainLoop.mainLoop().cam
    #screen = PEG_mainLoop.mainLoop().screen
    global imr,screen
    screen.blit(pygame.transform.rotate(imr['tempshape'].subsurface(linanim['tempshape'][frame]),rotation),location.getIntTuple())
    
def drawBuilding(color,sides):
    pass


#test script

if __name__ == "__main__":
    last = 0
    sound = PEG_sound.soundMan()
    while 1:
        try: fps = 1000.0/(pygame.time.get_ticks() - last)
        except: pass
        print "fps",fps,"time",(pygame.time.get_ticks() - last)
        last = pygame.time.get_ticks()
        #sound.playOnceQuickAndDirty(os.path.join("data","sfx01.wav"))
        
        drawShapeEn("BLUE",(pygame.time.get_ticks()/50)%5+4,0,Vector2d(50,50))
        
        #drawAnimCentered('+1',(pygame.time.get_ticks()/50)%3)
        
        #draw
        #=======================================================================
        # drawShapeEn(None,None,None,Vector2d(50,50),(pygame.time.get_ticks()/150)%2)
        # drawShapeEn(None,None,None,Vector2d(100,200),(pygame.time.get_ticks()/150)%2)
        # drawShapeEn(None,None,None,Vector2d(150,50),(pygame.time.get_ticks()/150)%2)
        # drawShapeEn(None,None,None,Vector2d(200,200),(pygame.time.get_ticks()/150)%2)
        # drawShapeEn(None,None,None,Vector2d(250,50),(pygame.time.get_ticks()/150)%2)
        # drawShapeEn(None,None,None,Vector2d(300,200),(pygame.time.get_ticks()/150)%2)
        #=======================================================================
        
        #rotation test
        #=======================================================================
        # rotationtest(None,None,None,Vector2d(50,50),(pygame.time.get_ticks()/10),(pygame.time.get_ticks()/150)%2)
        # rotationtest(None,None,None,Vector2d(100,200),(pygame.time.get_ticks()/10),(pygame.time.get_ticks()/150)%2)
        # rotationtest(None,None,None,Vector2d(150,50),(pygame.time.get_ticks()/10),(pygame.time.get_ticks()/150)%2)
        # rotationtest(None,None,None,Vector2d(200,200),(pygame.time.get_ticks()/10),(pygame.time.get_ticks()/150)%2)
        # rotationtest(None,None,None,Vector2d(250,50),(pygame.time.get_ticks()/10),(pygame.time.get_ticks()/150)%2)
        # rotationtest(None,None,None,Vector2d(300,200),(pygame.time.get_ticks()/10),(pygame.time.get_ticks()/150)%2)
        #=======================================================================
        
        #flip screen
        pygame.display.flip()
        screen.fill(FL_COLOR_BACKGROUND)


