import pygame
import PEG_mainLoop
import PEG_entity
from PEG_datatypes import *
from PEG_constants import *

#factory mojo
def quickEffect(id, pos = Vector2d(0,0),color = (255,255,255), radius = 300, sides = 3):
    if id == 1:
        return EffectNova(PEG_mainLoop.mainLoop().entityDict['EffectMenu'], pos,2000,radius,color,pointFunctionSquigglyPoly)
    if id == 2:
        return EffectThickExpandingGonNova(PEG_mainLoop.mainLoop().entityDict['EffectMenu'], pos,1000,radius,color,7,pointFunctionSquigglyPoly)
    if id == 3:
        return EffectAsteroidExplosion(PEG_mainLoop.mainLoop().entityDict['EffectMenu'], pos,3000,radius,color,3,pointFunctionSquigglyPoly)
    if id == 4:
        return EffectGameOver(PEG_mainLoop.mainLoop().entityDict['EffectMenu'], expiration = 5000)
    if id == 5:
        return EffectImplodingNova(PEG_mainLoop.mainLoop().entityDict['EffectMenu'], pos,2000,radius,color,pointFunctionExpandingGonNova)
    if id == 6:
        return EffectImplodingNova(PEG_mainLoop.mainLoop().entityDict['EffectMenu'], pos,500,radius,color,pointFunctionSquigglyPoly)
    if id == 7:
        return EffectAsteroidExplosion(PEG_mainLoop.mainLoop().entityDict['EffectMenu'], pos,3000,radius,color,1,pointFunctionSquigglyPoly)
    if id == 8:
        return EffectImplodingNova(PEG_mainLoop.mainLoop().entityDict['EffectMenu'], pos,2000,radius,color,pointFunctionExpandingGonNovaX(3))
    if id == 9:     #X sides imploding polygon for leveling up players
        return EffectImplodingNova(PEG_mainLoop.mainLoop().entityDict['EffectMenu'], pos,2000,radius,color,pointFunctionExpandingGonNovaX(sides))
    
def quickData(id, pos = Vector2d(0,0),color = (255,255,255), radius = 300, sides = 3):
    data = dict()
    data['type'] = "effect"
    data['id'] = id
    data['x'] = int(pos.x)
    data['y'] = int(pos.y)
    data['r'] = color[0]
    data['g'] = color[1]
    data['b'] = color[2]
    data['radius'] = radius
    data['sides'] = sides
    return data

class EffectMenu(PEG_entity.Entity):
    def __init__(self,exml=None):
        PEG_entity.Entity.__init__(self,Rect2d(0,0,0,0))
        self.effectList = list()
        self.deleteList = []
        self.screen = PEG_mainLoop.mainLoop().screen
        #self.effectList.append(EffectNova(self,Vector2d(100,100),4000,500,(255,255,255),pointFunctionSquigglyPoly))
        
    def update(self):
        for e in self.effectList:
            e.updateTime()
            e.update()
            
        self.deleteRoutine()
    def addEffect(self,effect):
        self.effectList.append(effect)
            
    def draw(self):
        for e in self.effectList:
            e.draw()
            
    def deleteEffect(self, e):
        self.deleteList.append(e)
    
    def deleteRoutine(self):
        for e in self.deleteList:
            self.effectList.remove(e)
        self.deleteList = []
    
class Effect:
    def __init__(self,parent, position = Vector2d(0,0), expiration = 0):
        self.startTime = pygame.time.get_ticks()
        self.lastUpdate = pygame.time.get_ticks()
        self.p = parent
        self.exp = expiration
        self.pos = position
    def update(self):
        pass
    def updateTime(self):
        self.lastUpdate = pygame.time.get_ticks()
        if self.lastUpdate > self.startTime + self.exp:
            self.deleteSelf()
    def deleteSelf(self):
        self.p.deleteEffect(self)
    def draw(self):
        pass
    
import PEG_speech
import pedo_lookup
class EffectGameOver(Effect):
    def __init__(self,parent,position = Vector2d(0,0),expiration = 999999,text = "Game Over"):
        Effect.__init__(self,parent,position,expiration)
        self.text = text
    def draw(self):
#===============================================================================
#        alpha = 40 + (self.lastUpdate - self.startTime)*200/2000
#        if alpha > 200: alpha = 200
#        pygame.draw.rect(
#                         self.p.screen,
#                         (0,0,0, 255),
#                         pygame.Rect(0,0,800,480),
#                         0
#                         )
#===============================================================================
        PEG_speech.Speech().setSize(100, pedo_lookup.fontList[3])
        PEG_speech.Speech().writeCentered(
                                          self.p.screen,
                                          Vector2d(400,240),
                                          self.text,
                                          COLOR_CHARTREUSE
                                          )
class EffectNova(Effect):
    def __init__(self,parent,position,expiration,maxRadius,color,pointFunction):
        Effect.__init__(self,parent,position,expiration)
        self.maxRadius = maxRadius
        self.color = color
        self.pointFunction = pointFunction
    def draw(self):
        radius = self.maxRadius*(self.lastUpdate - self.startTime)/self.exp
        pygame.draw.polygon(
                            self.p.screen,
                            self.color,
                            self.pointFunction(self.pos,radius),
                            1
                            )
                
#you should change this so it accepts a list of functions
#THIS FUNCTION IS BAAAAAAD just loook scool
class EffectThickExpandingGonNova(Effect):
    def __init__(self,parent,position,expiration,maxRadius,color,quantity,pointFunction):
        Effect.__init__(self,parent,position,expiration)
        self.maxRadius = maxRadius
        self.color = color
        self.pointFunction = pointFunction
        self.quantity = quantity
        
        self.asteroidList = list()
        for i in range(quantity):
            self.asteroidList.append( [Vector2d(0,0),getRandVector2d(self.maxRadius)*(1/self.exp)] )
    
    def update(self):
        for e in self.asteroidList:
            e[0] += e[1]
    def draw(self):
        radius = self.maxRadius*(self.lastUpdate - self.startTime)/self.exp
        for e in self.asteroidList:
            pygame.draw.polygon(
                            self.p.screen,
                            self.color,
                            self.pointFunction(self.pos,radius),
                            1
                            )
            
class EffectImplodingNova(Effect):
    def __init__(self,parent,position,expiration,maxRadius,color,pointFunction, sides = 5):
        Effect.__init__(self,parent,position,expiration)
        self.maxRadius = maxRadius
        self.color = color
        self.pointFunction = pointFunction
        self.sides = sides
    def draw(self):
        radius = self.maxRadius - self.maxRadius*(self.lastUpdate - self.startTime)/self.exp
        pygame.draw.polygon(
                            self.p.screen,
                            self.color,
                            self.pointFunction(self.pos,radius),
                            1
                            )
#===============================================================================
#        pygame.draw.polygon(
#                            self.p.screen,
#                            self.color,
#                            self.pointFunction(self.pos,radius+10),
#                            1
#                            )
#===============================================================================

class EffectImplodingGonNova(Effect):
    def __init__(self,parent,position,expiration,maxRadius,color,pointFunction, sides = 5):
        Effect.__init__(self,parent,position,expiration)
        self.maxRadius = maxRadius
        self.color = color
        self.pointFunction = pointFunction
        self.sides = sides
    def draw(self):
        radius = self.maxRadius - self.maxRadius*(self.lastUpdate - self.startTime)/self.exp
        pygame.draw.polygon(
                            self.p.screen,
                            self.color,
                            self.pointFunction(self.pos,radius, self.sides),
                            1
                            )

    

class EffectAsteroidExplosion(Effect):
    def __init__(self,parent,position,expiration,maxRadius,color,quantity,pointFunction):
        Effect.__init__(self,parent,position,expiration)
        self.maxRadius = maxRadius
        self.color = color
        self.pointFunction = pointFunction
        self.quantity = quantity
        
        self.asteroidList = list()
        for i in range(quantity):
            self.asteroidList.append( [self.pos + Vector2d(0,0),getRandVector2d(self.maxRadius)*(30.0/float(self.exp))] )
    
    def update(self):
        for e in self.asteroidList:
            e[0] += e[1]
            #print e[1].x
    def draw(self):
        for e in self.asteroidList:
            #print e[0].x
            pygame.draw.polygon(
                            self.p.screen,
                            self.color,
                            self.pointFunction(e[0],7),
                            1
                            )

import PEG_draw

def pointFunctionExpandingGonNovaX(sides):
    def f(pos,radius,arg01=None,arg02=None,arg03=None):
        return PEG_draw.getNgon(sides,radius,pos)
    return f
def pointFunctionExpandingGonNova(pos, radius, sides = 15, arg02 = None, arg03 = None):
    return PEG_draw.getNgon(sides,radius,pos)
def pointFunctionSquigglyPoly(pos, radius, min = 0.1, max = 0.3, arg03 = None):
    return convertToTupleList(map(pos.__add__,PEG_draw.getGaussianGon(radius, radius + 10, min, max)))
def moveFunctionIntermittentShake(): pass