import pygame
from PEG_datatypes import *
import PEG_mainLoop
import PEG_camera
import xml.dom.minidom
import PEG_helpers
from PEG_constants import *
from PEG_entity import *
import PEG_entity
import pedo_lookup
import PEG_server
import PEG_sound
import PEG_effects
import os, sys, traceback

class Network(PEG_entity.Entity):
    
    def __init__(self, exml = None):
        PEG_entity.Entity.__init__(self, Rect2d(0,0,0,0))
        
         #server mode or client mode
        self.server = False
        if exml and exml.hasAttribute("server"):
            if exml.getAttribute("server") == "True":
                self.server = True   
        #set up the server yo
        if self.server: self.mgr = PEG_server.ClntManager()
        else: self.mgr = PEG_server.SrvManager()
        self.mgr.start()        
        self.lastTimeWeGrabbedData = 0      #used for testing network speed, not important
        
        
        #GAME VARIABLES
        
        self.enDict = dict()        #contains all info about networked game entities
        self.destroyList = list()
        self.selection = None       #selection (with mouse) for additional informaiton display
        self.gameTime = "00:00"     #game time, either formatted and sent (server) or received and displayed (client)
        self.color = ''             #only relevent for client, set by SelectEn, represents what shape (color) in enDict is the player
        
        self.state = "waiting"                              #game state, they are ('waiting','starting','started','end') and repeats
        self.stateStartTime = pygame.time.get_ticks()       #when we started the last state, used by server

        self.sendFlag = True
        
    def __del__(self):
        try:
            self.mgr.killMe()
            self.mgr.join(1)
        except: print "network closing error -- can ignore this"
        
    def update(self):
        

        t1 = pygame.time.get_ticks()
        self.grabData()
        t1 = pygame.time.get_ticks() -t1
        
        t4 = pygame.time.get_ticks()
        #process coordinates
        if self.server:
            self.logic()
        t4 = pygame.time.get_ticks() -t4
        
        t2 = pygame.time.get_ticks()
        #TEMPORARY FOR ON SCREEN CONTROLLED VERISON
        for e in self.enDict.values():
            if hasattr(e,'controlled') and  e.controlled:
                e.update()
        t2 = pygame.time.get_ticks() -t2
                
        t3 = pygame.time.get_ticks()
        self.deleteEntities()
        t3 = pygame.time.get_ticks() -t3
                
        t5 = pygame.time.get_ticks()
        if self.server and self.sendFlag: 
            self.sendData()
            self.sendFlag = False
        else: self.sendFlag = True
        t5 = pygame.time.get_ticks() -t5
        
        if pygame.time.get_ticks()%10 == 1:
            print "grab data:", t1, "controll", t2, "delete", t3, "logic", t4, "sending", t5, "total", t1+t2+t3+t4+t5
        
    def grabData(self):
        #get data, interpret it, and create objects as needed
        #get all the messages
        recv = None
        try:
            self.mgr.reclock.acquire()
            recv = self.mgr.rec
            self.mgr.rec = []
            self.mgr.reclock.release()
        except: 
            print "data grab exception"
            try: self.mgr.reclock.release()
            except: print "reclock release exception"
        
        #read in data
        #Messages are in this form:
        #[0:letter representing message type][5:representing object id][21:object name][30:x][39:y][49:state]
        if len(recv) > 0:
#===============================================================================
#            if pygame.time.get_ticks()%40 == 1:
#                print "time since last network update :", pygame.time.get_ticks() - self.lastTimeWeGrabbedData
#            self.lastTimeWeGrabbedData = pygame.time.get_ticks()
#===============================================================================
            for m in recv:
                data = self.defaultParseMessage(m[0])
                
                #self.processNetMessage(data)
                
                #use this for silent failing.
                try:
                    pass
                    self.processNetMessage(data)
                except: 
                    print "data misformated: ", m[0], " ", data
#===============================================================================
#                    exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
#                    print "*** print_tb:"
#                    traceback.print_tb(exceptionTraceback, limit=1, file=sys.stdout)
#                    print "*** print_exception:"
#                    traceback.print_exception(exceptionType, exceptionValue, exceptionTraceback,
#                                              limit=2, file=sys.stdout)
#===============================================================================


    def changeState(self,state):
        self.state = state
        self.stateStartTime = pygame.time.get_ticks()
        
        data = dict()
        if state == "starting":
            data['type'] = "state"
            data['state'] = "starting"
        elif state == "end":
            #determine the winner
            #adding sides
            def isTeamA(en): return en.getTeam == "A"
            def isTeamB(en): return en.getTeam == "B"
            sumA = sumB = 0
            for e in self.enDict.values():
                if e.team == "A":
                    sumA += e.sides
                elif e.team == "B":
                    sumB += e.sides           
            winner = "A"
            if sumB > sumA: winner = "B"
            if sumB == sumA: winner = "TIE"
            data['type'] = "state"
            data['state'] = "gameover"
            data['winner'] = winner
            data['Ateam'] = sumA
            data['Bteam'] = sumB
        if len(data) > 0:
            network = PEG_mainLoop.mainLoop().entityDict['Network']
            network.mgr.send( network.makeDefaultMessage(data))
            
        print "game state changed to: ", state
        
    def logic(self):
        if self.state == "waiting":
            self.gameTime = ""
            #TODO if all players identified AND power source located then change to state = started
            return
        if self.state == "starting":
            timeDiff = pygame.time.get_ticks() - self.stateStartTime
            if timeDiff > 10000:
                self.changeState("started")
                return
            try: self.gameTime = 10 - timeDiff / 1000 
            except: self.gameTime = 10
            return
        if self.state == "started":
            timeDiff = pygame.time.get_ticks() - self.stateStartTime
            if timeDiff > GAME_PLAY_TIME:
                self.changeState("end")
                return
            
            #figure out the time
            untilEnd = (GAME_PLAY_TIME - timeDiff)/1000
            minutes = int(untilEnd/60)
            seconds = untilEnd - minutes*60
            self.gameTime = str(minutes)+':'+str(seconds)
            
            
            self.checkTraps()
            #revive players
            self.revivePlayers()
            return
        
        if self.state == "end":
            timeDiff = pygame.time.get_ticks() - self.stateStartTime
            if timeDiff > 10000:
                #TODO if no clietns have dropped
                self.changeState("starting")
                #TODO delete all non shape entities
                for e in self.enDict:
                    if isinstance(e,PEG_entity.ShapeEn):
                        e.sides = 3
                
            self.gameTime = "00:00"        
            return
            
    
    def checkTraps(self):
        for e in self.getShapeEnList():
            for f in self.getBuildingEnList():
                if f.sides == 3 and (e.pos.getPosition() - f.pos.getPosition()).magnitude() < TRAP_RADIUS:
                    print "trap triggered"
                    f.sides -= 1
                    e.attackedBy(f)
                
    #TODO move revive players into processArduino (so it happens when you do some revive action
    def revivePlayers(self):
        for e in self.enDict.values():
            if e.sides == 0:
                #TODO check distance from power source --> revive
                #do we give zero resources?
                if (e.pos.getPosition() - self.enDict['ThePSource'].pos.getPosition()).magnitude() < POWERSOURCE_ACTIVE_RADIUS:
                    e.sides = 3 
                    e.state = "alive"
                    print e.name, "revived"
                    #animation
                    args = [8, e.pos.getPosition(), e.color, ACTION_RADIUS]
                    self.mgr.send(self.makeDefaultMessage(PEG_effects.quickData(*args)))
        
    def processNetMessage(self, data):
        if data['type'] == "position":
            self.parseEnData(data)          #has server specific routine inside
        if data['type'] == "time":
            self.gameTime = data['time'][1:len(data['time'])]   #time data is buffered with a character so we can start it with a 0
        if not self.server and data['type'] == "effect":        #client specific
            #should have position, id, etc 
            PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect(PEG_effects.quickEffect(
                                                                                               int(data['id']), 
                                                                                               Vector2d(int(data['x']),int(data['y'])),
                                                                                               (int(data['r']),int(data['g']),int(data['b'])), 
                                                                                               int(data['radius']),
                                                                                               int(data['sides'])
                                                                                               ))
        if not self.server and data['type'] == "state":        #client specific
            menu = PEG_mainLoop.mainLoop().entityDict["MenuEn"]
            if data['state'] == "gameover":
                menu.gameover = True
                menu.winner = data['winner']
                menu.Ascore = data['Ateam']
                menu.Bscore = data['Bteam']
            elif data['state'] == "starting":
                menu.gameover = False
                
        if self.server and 'relay' in data:                     #server specific
                del data['relay']
                self.mgr.send(self.makeDefaultMessage(data))
                #print "relayed", data
        
        if self.server and data['type'] == "event":             #server specific
            if data['event'] == "arduino":
                if 'ThePSource' in self.enDict:
                    self.processArduinoEvent(data)
            
        #FIXK THIS
        if self.server and data['type'] == "init":              #server specific
            try: 
                m[1].name = data['name']
                print data['name'], " client is set"
            except: pass
            
            #now we check if all clients have been identified
            #this is kind of bad because we don't wait for extra clients to join, too bad
            #add maybe a 10 second grace period
            flag = True
            self.mgr.clntListlock.acquire()
            for e in self.mgr.clntList:
                if e.name == None:
                    flag = False
                    break
            self.mgr.clntListlock.release()
#===============================================================================
#            if flag:
#                self.changeState("starting")
#===============================================================================
    
    def processArduinoEvent(self,data):
        #try:
        print "got", data['arduino'], "from", data['name']
        #whirly bird for attacking
        if data['arduino'] == "sweep":
            for e in [item for item in self.enDict.values() if hasattr(item,"attackedBy")]:
                if self.enDict[data['name']].team != e.team:
                    if (self.enDict[data['name']].pos.getPosition() - e.pos.getPosition()).magnitude() < ACTION_RADIUS:
                        print self.enDict[data['name']].name, "attacks", e.name, "successfully"
                        e.attackedBy(self.enDict[data['name']])
                        #play effect
                        args = [7, e.pos.getPosition(), pedo_lookup.colorMap[data['name']], ACTION_RADIUS]
                        #PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect(PEG_effects.quickEffect(*args))
                        self.mgr.send(self.makeDefaultMessage(PEG_effects.quickData(*args)))
                        
        #tomahawk for resources and construction
        if data['arduino'] == "tomahawk":
            
            flag1 = True
            for e in self.getBuildingEnList():
                if e.sides >= 5:
                    if (self.enDict[data['name']].pos.getPosition() - e.pos.getPosition()).magnitude() < POLYFACTORY_ACTIVE_RADIUS:
                        if self.enDict[data['name']].resources == self.enDict[data['name']].sides:
                            print self.enDict[data['name']].name, "has POLYFACTORIZED!!!!"
                            args = [9, self.enDict[data['name']].pos.getPosition(), self.enDict[data['name']].color, ACTION_RADIUS,self.enDict[data['name']].sides+1]
                            self.mgr.send(self.makeDefaultMessage(PEG_effects.quickData(*args)))
                            self.enDict[data['name']].sides += 1
                            self.enDict[data['name']].resources = 0
                            flag1 = False
                            break
            if flag1 and (self.enDict[data['name']].pos.getPosition() - self.enDict['ThePSource'].pos.getPosition()).magnitude() < POWERSOURCE_ACTIVE_RADIUS:
                print self.enDict[data['name']].name, "gathers 1 resource"
                self.enDict[data['name']].addResource()
                args = [5, self.enDict[data['name']].pos.getPosition(), self.enDict[data['name']].color, ACTION_RADIUS]
                sData = PEG_effects.quickData(*args)
                self.mgr.send(self.makeDefaultMessage(sData))
            elif flag1 and self.enDict[data['name']].resources > 0:  #TODO change to 0, keep this way for testing
                self.enDict[data['name']].resources -= 1
                flag2 = True
                print self.enDict.values()
                for e in self.getBuildingEnList():
                    if (self.enDict[data['name']].pos.getPosition() - e.pos.getPosition()).magnitude() < BUILDING_RADIUS:
                        if self.enDict[data['name']].team == e.team:
                            print e.name, "upgraded by", data['name']
                            e.addSide()
                            flag2 = False
                            args = [9, e.pos.getPosition(), e.color, ACTION_RADIUS,e.sides+1]
                            self.mgr.send(self.makeDefaultMessage(PEG_effects.quickData(*args)))
                            break
                #no upgrade, then make new structure
                if flag2:
                    print self.enDict[data['name']], "created new building"
                    sdata = dict()
                    sdata['type'] = "position"
                    sdata['entity'] = "BuildingEn"
                    sdata['x'] = self.enDict[data['name']].pos.x
                    sdata['y'] = self.enDict[data['name']].pos.y
                    #TODO make some system for creating unique names
                    sdata['name'] = random.randint(0,5000) 
                    sdata['team'] = self.enDict[data['name']].team
                    sdata['sides'] = 1
                    self.processNetMessage(sdata)
                    #bad but w/e
                    self.enDict[sdata['name']].team = sdata['team']
                    args = [9, self.enDict[data['name']].pos.getPosition(), pedo_lookup.colorMap[sdata['team']], 3]
                    self.mgr.send(self.makeDefaultMessage(PEG_effects.quickData(*args)))
        #TODO hit explosion thing I dunno
        #play some effect for now?
        if data['arduino'] == "whirlybird":
            #TODO detect mode????
            #for now everything is visible
            pass
        #except: print "arduino event failed"
        
    def sendData(self):
        #for each players
        #calculate game time based on ticks
        #send game time
        #we send gametime to everybody
        data = dict()
        data['type'] = 'time'
        data['time'] = 'a' + str(self.gameTime)     #we buffer the time with a character so we can start it with a 0
        network = PEG_mainLoop.mainLoop().entityDict['Network']
        network.mgr.send( network.makeDefaultMessage(data) )
        
        #send the object to everybody
        #TODO selective sending with player knowledge dict... for future version
        for e in self.enDict.values():
            if hasattr(e, 'updateData'):
                e.updateData()
            network.mgr.send( network.makeDefaultMessage(e.data) )
                                 
    def processInput(self):
        #determines which object has been selected. Not really important for server
        mousePos = pygame.mouse.get_pos()
        for e in PEG_mainLoop.mainLoop().eventList:
            if e.type == pygame.MOUSEBUTTONUP:
                for e in self.enDict.values():
                    if e.visible and e.pos.getPosition().distance(Vector2d(mousePos[0],mousePos[1])) < PLAYER_DRAW_RADIUS:
                        self.selection = e
                        print "you have selected: ", e.name
                        break
                    else: self.selection = None
                                                                                                  
                                                                                                 
                        
        
    def parseEnData(self,d):
        if d['name'] not in self.enDict:
            try:
                exml = xml.dom.minidom.parseString("<entity type=\"" + d['entity']
                                                   + "\" x=\"" + str(d['x'])
                                                   + "\" y=\"" + str(d['y'])
                                                   + "\" name=\"" + str(d['name'])
                                                   + "\" size=\"3"
                                                   +"\" />").getElementsByTagName("entity")[0]
                                                   
                self.enDict[d['name']] = pedo_lookup.enTables(exml)
                print d['entity'], ": ", d['name'], " created!"
                
#===============================================================================
#                try:
#                    if 'team' in data:
#                        self.enDict[d['name']].team = data['team']
#                except: pass
#===============================================================================
                
                #USER CONTROLLED STUFF
                if self.color == d['name']:
                    self.enDict[d['name']].controlled = True
                    print "self identified as: ", d['name']
            except: 
                    print "data misformated: ", d
                    exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
                    print "*** print_tb:"
                    traceback.print_tb(exceptionTraceback, limit=1, file=sys.stdout)
                    print "*** print_exception:"
                    traceback.print_exception(exceptionType, exceptionValue, exceptionTraceback,
                                              limit=2, file=sys.stdout)
                
        
        try:
            self.enDict[d['name']].data = d
        except: print d['name']," data set failed"
        try:            
            self.enDict[d['name']].updatePos()
        except: print d['name'], "update pos failed"
         
    def makeDefaultMessage(self, d):
        msg = ''
        for s in d.keys():
            #need to fill s with 0s
            msg += s.zfill(20)
            msg += str(d[s]).zfill(20)
        #msg += "00000000000000000000000000000000000000000000000000000000000000000000000000000000000"
        return msg
    
    def send(self,data):
        self.mgr.send(self.makeDefaultMessage(data))
        
    def defaultParseMessage(self,msg):
        """parses message in format
        [20:client name][40:msg type][60-440: data]
        msg: string
        returns (name,type,data[])
        """
        try:
            data = dict()
            #FORMAT AS DICTIONARY 20: DATA NAME 40: DATA VALUE
            for i in range(20):
                #if data is not empty
                if msg[0+i*40:20+i*40].lstrip('0') != "":
                    if msg[20+i*40:40+i*40].lstrip('0') == "":
                        data[msg[0+i*40:20+i*40].lstrip('0')] = 0   #default value when no data parameter is given. Maybe should be empty string or nothing?
                    else:
                        if msg[20+i*40:40+i*40].lstrip('0').isdigit() or msg[20+i*40:40+i*40].lstrip('0').lstrip('-').isdigit():
                            data[msg[0+i*40:20+i*40].lstrip('0')] = int(msg[20+i*40:40+i*40].lstrip('0'))
                        else:
                            data[msg[0+i*40:20+i*40].lstrip('0')] = msg[20+i*40:40+i*40].lstrip('0')
                else: break #no need to continue once we run out of data
            return data
        except: print "msg parsing failed"
    
    def getShapeEnList(self):
        shapeList = list()
        for e in self.enDict.values():
            if isinstance(e,PEG_entity.ShapeEn):
                shapeList.append(e)
        #print len(shapeList)
        return shapeList
    def getBuildingEnList(self):
        shapeList = list()
        for e in self.enDict.values():
            if isinstance(e,PEG_entity.BuildingEn):
                shapeList.append(e)
        #print len(shapeList)
        return shapeList
    
    def detectHit(self):
        #inner radiusQ
        for e in self.enDict.itervalues():
            for f in self.enDict.itervalues():
                if e != f:
                    if e.pos.getPosition().distance(f.pos) < INNER_RADIUS:
                        pass#PEG_sound.soundMan().playOnceQuickAndDirty(os.path.join('data','sfx01.wav'))
    
    def detectCircling(self):
        #outer radius
        for e in self.enDict.itervalues():
            for f in self.enDict.itervalues():
                if e != f:
                    if f.name not in e.playerAngleDict:
                        e.playerAngleDict[f.name] = []
                        for i in range(PLAYER_ANGLE_QUEUE_LEN):
                            e.playerAngleDict[f.name].append(0)
                    if e.pos.getPosition().distance(f.pos) < OUTER_RADIUS:                   
                        angle = getAngle3pt(e.lastPos[len(e.lastPos)-2], f.lastPos[len(f.lastPos)-2], e.lastPos[len(e.lastPos)-1])
                        e.playerAngleDict[f.name].append(angle)
                        e.playerAngleDict[f.name].pop(0)
                        pygame.draw.lines(PEG_mainLoop.mainLoop().screen,(0,0,0),False,(e.pos.getPosition().getIntTuple(), f.pos.getPosition().getIntTuple()))
                    else:
                        #we gain no angles when outside OUTER RADIUS so pop last and push a 0
                        e.playerAngleDict[f.name].append(0)
                        e.playerAngleDict[f.name].pop(0)
                        pass
                    try: 
                        #we try as f may not have e set up in his playerAngleDict yet
                        if math.fabs(reduce(add, e.playerAngleDict[f.name])) - math.fabs(reduce(add, f.playerAngleDict[e.name])) > ENCIRCLE_RADIANS:
                            print e.name, " has lapped", f.name
                            f.state = "destroy"
                            f.data["state"] = "destroy"
#===============================================================================
#                            PEG_helpers.quickFcnCaller().callOnce(
#                                                                  23042729749234,
#                                                                  PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect,
#                                                                  [PEG_effects.quickEffect(3, f.pos.getPosition(), (255,255,255), 300),]
#                                                                  )
#===============================================================================
                    except: pass
                    #except Exception as inst:
                        #print type(inst)     # the exception instance

    def deleteEntities(self):
        for e in self.enDict.values():
            if e.state == "destroy":
                self.destroyList.append(e)
        
        for s in self.destroyList:
            try: del self.enDict[s]
            except: print "delete entity failed"
        self.destroyList = list()
        
    def draw(self):
#===============================================================================
#        if self.selection and self.color:
#            self.enDict[self.color].drawConnectingLines(self.selection)
#            self.enDict[self.color].drawAnglePie(self.selection)
#===============================================================================
        for e in self.enDict.itervalues():
            e.draw()