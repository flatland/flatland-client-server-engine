import pygame
from PEG_datatypes import *

def livesIn(point,rect):
    """see if point lies in rect
    
    point: tuple or vect2d
    rect: Rect2d"""
    #shitty type checking lol
    p = None
    try: p = Vector2d(point.x,point.y)
    except: p = Vector2d(point[0],point[1])
    
    return collided(rect,Rect2d(p.x,p.y,0,0))

def collided(r1, r2):
    """checks collision between two Rect2d and returns bool
    
    r1, r2: Rect2d
    returns: bool"""
    AL = r1.x
    AR = r1.x + r1.w
    AT = r1.y
    AB = r1.y + r1.h
    BL = r2.x
    BR = r2.x + r2.w
    BT = r2.y
    BB = r2.y + r2.h
    #check left and right
    if AR < BL or BR < AL:
        return False
    if AB < BT or BB < AT:
        return False
    return True

def collideSide(r1, r2):
    #make sure there was a collision
    if collided(r1,r2) == False:
        return (0,0)
    AL = r1.x
    AR = r1.x + r1.w
    AT = r1.y
    AB = r1.y + r1.h
    BL = r2.x
    BR = r2.x + r2.w
    BT = r2.y
    BB = r2.y + r2.h
    #if A RIGHT is in B LEFT
    if AR-BL < BR-AL:
        #if A BOTTOM is in B TOP
        if AB-BT < BB-AT:
            if AR-BL < AB-BT:
                return Vector2d(AR-BL,0)
            else:
                return Vector2d(0,AB-BT)
        else:
            if AR-BL < BB-AT:
                return Vector2d(AR-BL,0)
            else:
                return Vector2d(0,-(BB-AT))   
    else:
        #if A BOTTOM is in B TOP
        if AB-BT < BB-AT:
            if BR-AL < AB-BT:
                return Vector2d(-(BR-AL),0)
            else:
                return Vector2d(0,AB-BT)
        else:
            if BR-AL < BB-AT:
                return Vector2d(-(BR-AL),0)
            else:
                return Vector2d(0,-(BB-AT))
    
def truncateToMultiple(number, multiple):
    """truncates number to greatest multiple of multiple not greater than number
    
    number: number to truncate
    multiple: integer to truncate at multiple of"""
    return int(number/multiple)*multiple
    
def duck():
    print "quack"
    

class quickFcnCaller:
    class __impl:
        def __init__(self):
            self.callerDict = dict()
            self.callTimeBuffer = 200 #milliseconds since last call for something to be considered a new call
        def callOnce(self,id,function,args):
            if id not in self.callerDict:
                self.callerDict[id] = 0
            if pygame.time.get_ticks()-self.callerDict[id] > self.callTimeBuffer:
                function(*args)
            self.callerDict[id] = pygame.time.get_ticks()
        def resetId(self,id):
            self.callerDict[id] = 0
                
            
    __instance = None
    def __init__(self):
        if quickFcnCaller.__instance is None:
            quickFcnCaller.__instance = quickFcnCaller.__impl()        
        self.__dict__['_quickFcnCaller_instance'] = quickFcnCaller.__instance
    def __getattr__(self, attr): return getattr(self.__instance, attr)
    def __setattr__(self, attr, value): return setattr(self.__instance, attr, value) 
        