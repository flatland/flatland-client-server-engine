import socket
import pygame
import threading
import random
import time
import select
import PEG_mainLoop

from PEG_constants import *


CONNECTION_DROP_TIMEOUT = 1000


def appendMessageLength(msg):
    return str(len(msg)).zfill(6) + msg

def makeMessage(data):
    msg = ''
    for e in data.keys():
        if msg:
            msg += "$"
        msg += str(e)
        msg += "$"
        msg += str(data[e])
    #seems to be wroking correctly
    #print data, msg, msg.split("$")
    return msg

def parseMessage(msg):
    #do it
    #use & to split chunks, $ to split subchunks
    datalist = list()
    msgs = msg.split("&")
    for e in msgs:
        d = dict()
        m = e.split("$")
        for i in range(len(m)/2):
            if m[i*2+1].lstrip('-').isdigit() :
                d[m[i*2]] = int(m[i*2+1])
            else: d[m[i*2]] = m[i*2+1]
        datalist.append(d)
    return datalist
            

class PSocket():
    def __init__(self,sock):
        self.sock = sock
        self.name = None
        self.destroy = False
        
        #individual must send messages
        self.must = ''
        self.mustlock = threading.Lock()
    def __del__(self):
        print "closing socket"
        self.sock.close()
        print "closed socket"
def getSocket(psock):
    return psock.sock
def getFilterSocket(name):
    def filterSocket(psock):
        return psock.name == name
    return filterSocket

class ClntManager(threading.Thread):
    PORT = 51007
    #these are 'public methods'
    def send(self,datalist):
        msg = ''
        for e in datalist:
            if msg:
                msg += "&"
            msg += makeMessage(e)
        #seems to be working
        #print datalist, msg
        #print parseMessage(msg)
        self.sndlock.acquire()
        self.snd += msg
        self.sndlock.release()
    def mustSend(self,data):
        msg = ''
        for e in data:
            if msg:
                msg += "&"
            msg += makeMessage(e)
        self.clntListLock.acquire()
        for e in self.clntList:
            e.mustlock.acquire()
            e.must += msg   
            e.mustlock.release()
        self.clntListLock.release()
        #TODO append tomust send list for each PSOCKET
    def clearSend(self):
        self.sndlock.acquire()
        self.snd = ''
        self.sndlock.release()
        
    def __init__(self):
        threading.Thread.__init__(self)
        self.initialized = False
        self.destroy = False
        self.clntList = []
        self.clntListLock = threading.Lock()
        self.destroyList = []
        
        #snd contains a big string to be sent to all clients. THis is constantly updated by mainloop and the most recent data is sent to all clients who have just sent data
        self.snd = ''
        self.sndlock = threading.Lock()
        #list of received data...... for the server, since it runs so fast, we ignore nothing in here, no optimization is made to remove doubles although we CAN add that if we are smart....
        self.rec = list()
        self.reclock = threading.Lock()
        
    def getPSock(self,sock):
        #if we really want this to be faster we put the thing in a map or something...
        for e in self.clntList:
            if e.sock == sock:
                return e
    def getSocketList(self,name = None):
        if name == None:
            return map(getSocket,self.clntList)
        else:
            return map(getSocket,filter(getFilterSocket(),self.clntList))
    def getMessage(self,sock):
        msg = sock.recv(6)
        if msg == '':
            self.getPSock(sock).destroy = True
            return ''
        return sock.recv(int(msg))
    def run(self):
        while 1:
            if self.destroy: break
            if not self.initialized:
                try:
                    self.init()
                except:
                    print "server initialization failed... Will try again."
            else:
                wtr = self.getSocketList()
                wtr.append(self.sock)
                rtr,rtw,ie = select.select(wtr,[],[],0)
                for e in rtr:
                    if e == self.sock:
                        try:
                            conn,addr = self.sock.accept()
                            print "created connection client object", addr
                            #append connection
                            self.clntListLock.acquire()
                            self.clntList.append(PSocket(conn))
                            self.clntListLock.release()
                        except:
                            print "sock accept failed"
                    else:
                        try:
                            rmsg = parseMessage(self.getMessage(e))
                            self.reclock.acquire()
                            self.rec.extend(rmsg)
                            self.reclock.release()
                            
                            self.sndlock.acquire()
                            e.send(appendMessageLength(self.snd + '&' + self.getPSock(e).must))
                            self.getPSock(e).mustlock.acquire()
                            self.getPSock(e).must = ''
                            self.getPSock(e).mustlock.release()
                            self.sndlock.release()
                                    
                        except:
                            self.getPSock(e).destroy = True
                            print e, "destroyed"
                self.destroyCycle()
                    
            
    def destroyCycle(self):
        for e in self.clntList:
            if e.destroy:
                self.destroyList.append(e)
        self.clntListLock.acquire()
        for e in self.destroyList:
            print "socket",e.name, e, "destroyed"
            self.clntList.remove(e)
        self.clntListLock.release()
        self.destroyList = list()
        
    def killMe(self):
        self.destroy = True
        
    def init(self):
        print "initializing server"
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self.sock.bind((socket.gethostbyname(socket.gethostname()),self.PORT))
        self.sock.bind(('',self.PORT))
        self.sock.setblocking(0)
        self.sock.listen(5)
        self.initialized = True
        print "server initialized"
        

class SrvManager():
    def __init__(self):
        self.snd = ''
        self.sock = None
        self.initialized = False
        
        self.rec = ['',]
        self.reclock = threading.Lock()
        
    def send(self,datalist):
        #adds data to one big send message that gets sent and emptied once a cycle
        for e in datalist:
            if e:
                if self.snd:
                    self.snd += "&"
                self.snd += makeMessage(e)
                
    def killMe(self):
        pass
                
    def update(self):
        #probably blocks
        if not self.initialized:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print "trying to connect..."
            try:
                self.sock.connect((SERVERIP, 51007))
            except:
                print "Connecting failed, trying again in 1 seconds"
                pygame.time.wait(1000)
                return
            print "connected succesfully"
            self.sock.setblocking(0)
            self.initialized = True
        self.rec = ['',]
        rtr,rtw,ie = select.select([self.sock],[self.sock],[self.sock],10)
        try:
            if len(rtw) > 0:
                if not self.snd:
                    self.snd = "type$filler"
                self.snd = appendMessageLength(self.snd)
                self.sock.send(self.snd)
                self.snd = ''
            if len(rtr) > 0:
                msg = self.sock.recv(6)
                if msg == '':
                    print self.sock.getsockname(), " destroyed!"
                    self.init = False
                    self.sock = None
                else:
                    msg = self.sock.recv(int(msg))
                    #we should only be getting one message from server each cycle
                    self.rec = parseMessage(msg)
        except:
            print "network failed, resarting now"
            self.initialized = False
        