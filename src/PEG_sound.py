import pygame

class soundMan:
    class __impl:
        def __init__(self):
            self.soundList = dict()
            self.lastPlayQuickAndDirty = pygame.time.get_ticks()
            
        def loadSound(self,filename):
            """loads sound filename and puts it on the flywheel
            
            filename: string"""
            if filename not in self.soundList:
                self.soundList[filename] = pygame.mixer.Sound(filename)
                print "sound loaded: ", filename
            
            
        def play(self, filename):
            if filename not in self.soundList:
                self.loadSound(filename)
            self.soundList[filename].play()
            
        def playOnceQuickAndDirty(self,filename):
            if pygame.time.get_ticks() - self.lastPlayQuickAndDirty > 1000:
                print "playing sound"
                self.play(filename)
                self.lastPlayQuickAndDirty = pygame.time.get_ticks()
            
    __instance = None
    def __init__(self):
        """ create instance """
        #check if instance exists and create it
        if soundMan.__instance is None:
            soundMan.__instance = soundMan.__impl()
            
        
        #store instance reference as the only member in the handle???
        self.__dict__['_soundMan_instance'] = soundMan.__instance
    
    def __getattr__(self, attr):
        """delegate access to implementation"""
        return getattr(self.__instance, attr)
    
    def __setattr__(self, attr, value):
        """delegate access to implementation"""
        return setattr(self.__instance, attr, value) 
        