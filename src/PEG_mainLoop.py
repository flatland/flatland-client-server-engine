from PEG_camera import *
from PEG_helpers import *
from pedo_player import *

import pedo_lookup
import PEG_server
import PEG_entity
import PEG_effects
import PEG_serial
import PEG_network
import xml.dom.minidom


class mainLoop:
    class __impl:
        
        #BAD these are CLASS variables and not instance variables though this is SAFE as this is a singleton object
        #public data
        screen = None
        cam = None
        eventList = []
        entityList = []
        #redundant entries from entityList, used for quick access of important components
        #there is no cleanup for this dict as we assume these components are never destoryed until the end of mainLoop
        entityDict = dict()
        deleteList = []
        cLevel = 0
        wLevel = 1
        
        def __init__(self, sfc):
            self.screen = sfc
            self.cam = Camera(sfc)
            self.fps = 0
            self.lastUpdate = 0
            
        def __del__(self):
            pass
            
        def loop(self):
            try: self.fps = 1000/(pygame.time.get_ticks() - self.lastUpdate)
            except: pass
            
            self.lastUpdate = pygame.time.get_ticks()
            
            #load level
            if self.cLevel != self.wLevel:
                self.loadLevel(self.wLevel)
                self.cLevel = self.wLevel
                pass
            
            #get (ALL) events and clear it
            self.eventList = pygame.event.get()
            pygame.event.clear()
            
            
            self.cam.moveTo(Vector2d(400,240))
                    
            #t1 = pygame.time.get_ticks()
            for e in self.entityList:
                e.update()
                
            #if pygame.time.get_ticks() % 10 == 1:
            #    print "update time: ", pygame.time.get_ticks() - t1
                          
            #self.handleCollision()
            
            self.deleteRoutine()

            #t2 = pygame.time.get_ticks()
            for e in self.entityList:
                e.draw()
            #if pygame.time.get_ticks() % 10 == 1:
            #   print "draw time: ", pygame.time.get_ticks() - t2 
            
        def handleCollision(self):
            for e in self.entityList:
                if isinstance(e, MovingEntity):
                    for f in self.entityList:
                        if isinstance(f, StaticEntity):
                            if collided(e.pos, f.pos):
                                e.collide(f)
                                pass
        
        def deleteEntity(self, e):
            self.deleteList.append(e)
        
        def deleteRoutine(self):
            for e in self.deleteList:
                self.entityList.remove(e)
            self.deleteList = []
        
        def loadLevel(self,lvl):
            topxml = xml.dom.minidom.parse("level.xml")
            lvlxml = None
            for n in topxml.getElementsByTagName('level'):
                if n.hasAttribute("id") and n.getAttribute("id") == str(lvl):
                    lvlxml = n
                    break
            if lvlxml == None:
                print "no such level exists!"
                return
            for n in lvlxml.getElementsByTagName('entity'):
                if n.hasAttribute("type"):
                    ten = pedo_lookup.enTables(n)
                    #WE WILL SEPARATE THIS FROm XML LATER
                    if isinstance(ten,PEG_network.Network):
                        self.entityDict["Network"]= ten
                    if isinstance(ten,MenuEn):
                        self.entityDict["MenuEn"]= ten
                    if isinstance(ten,PEG_effects.EffectMenu):
                        self.entityDict["EffectMenu"]= ten
                    try:
                        if isinstance(ten,PEG_serial.SerialEntity):
                            self.entityDict["SerialEntity"]= ten
                    except: 
                        print "could not find serial"
                    self.entityList.append(ten)
                    
        
        def saveState(self,lvl):
            topxml = xml.dom.minidom.parse("level.xml")
            #cerate a new node
            lvlxml = topxml.createElement("level")
            #topxml.appendChild(lvlxml)
            lvlxml.setAttribute("id",str(lvl))
            for e in self.entityList:
                if e.getxml():
                    lvlxml.appendChild(e.getxml())
                else: print "get xml failed on object: ", e
            print lvlxml.toprettyxml()
            f = open('level.xml','w')
            f.write(topxml.toprettyxml())
                   
    __instance = None
    
    def __init__(self,sfc = None):
        """ create instance """
        #check if instance exists and create it
        if mainLoop.__instance is None:
            if sfc == None:
                print "mainLoop instantiation requires surface"
            mainLoop.__instance = mainLoop.__impl(sfc)
            
        
        #store instance reference as the only member in the handle???
        self.__dict__['_mainLoop_instance'] = mainLoop.__instance
    
    def __getattr__(self, attr):
        """delegate access to implementation"""
        return getattr(self.__instance, attr)
    
    def __setattr__(self, attr, value):
        """delegate access to implementation"""
        return setattr(self.__instance, attr, value) 
        