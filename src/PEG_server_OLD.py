#server module prototype
#TODO cleanup, threads need to be able to close cleanly and call close command on socket

import socket
import pygame
import threading
import random
import time
from PEG_constants import *

CONNECTION_DROP_TIMEOUT = 1000


def makeMessage(msg):
    return str(len(msg)).zfill(6) + msg

class ClntManager(threading.Thread):
    PORT = 51007
    clntList = []
    clntListlock = threading.Lock()
    def __init__(self):
        threading.Thread.__init__(self)
        self.initialized = False
        self.destroy = False
        
        self.snd = ''
        self.sndlock = threading.Lock()
        self.rec = list()
        self.reclock = threading.Lock()
        
    def send(self, msg):
        self.sndlock.acquire()
        self.snd += makeMessage(msg)
        self.sndlock.release()
        
    def sendTo(self,msg,name):
        #this may block for a while, if so, you know what you need to do UGGGG
        for j in self.clntList:
            if j.name == name:
                j.sndlock.acquire()
                j.snd += msg
                j.sndlock.release()
        
        
    def run(self):
        while 1:
            time.sleep(0.03)
            if self.destroy: break
            if not self.initialized:
                try:
                    self.init()
                except:
                    print "server initialization failed... Will try again."
            else:
                #blocking so hopefully does not waste too many cycles (i.e. hope it makes the thread sleep)
                self.sock.listen(1)
                try:
                    conn,addr = self.sock.accept()
                    conn = ClntNB(conn, self)
                    print "created connection client object"
                    #append connection
                    self.clntList.append(conn)
                    #start their respective threads
                    conn.start()
                    #some sort of cleanup routine in case clients need to be destroyed
                except:
                    pass
            
            if len(self.clntList) == 0:
                self.sndlock.acquire()
                self.snd = ''
                self.sndlock.release()
            
            for c in self.clntList:
                #relay send data
                #move lock inside of loop if this causes mainloop to freeze
                self.reclock.acquire()
                for j in self.clntList:
                    j.reclock.acquire()
                    recmsg = j.rec
                    j.rec = []
                    j.reclock.release()
                    #alternative, we would lock here
                    if len(recmsg)  > 0:
                        self.rec.extend(recmsg)
                    del recmsg
                self.reclock.release()
                
                self.sndlock.acquire()
                sndmsg = self.snd
                self.snd = ''
                self.sndlock.release()
                for j in self.clntList:
                    j.sndlock.acquire()
                    j.snd += sndmsg
                    j.sndlock.release()
                    
                
                if c.destroy and not c.isAlive():
                    #TODO: grab data from dead socket
                    print "thread ", c.id, " destroyed" 
                    self.clntList.remove(c)
            
    def killMe(self):
        self.destroy = True
        
    def init(self):
        print "initializing server"
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self.sock.bind((socket.gethostbyname(socket.gethostname()),self.PORT))
        self.sock.bind(('',self.PORT))
        self.sock.setblocking(0)
        self.initialized = True
        print "server initialized"
    
    
    def relayData(self):
        """relays data turning server into a hub that does no processing
        """
        #UNTESTED
        #takes all received data for each client and sends it to every client
        for c in self.clntList:
            for j in self.clntList:
                if j != c:
                    #send j's data to c
                    c.sndlock.acquire()
                    for m in j.rec:
                        c.snd += makeMessage(m)
                    c.sndlock.release()
                
    
class SrvManager(threading.Thread):
    """this thread is for the client, it connects to ClntManager
    """
    PORT = 51007
    def __init__(self):
        threading.Thread.__init__(self)
        
        self.snd = ''
        self.sndlock = threading.Lock()
        self.rec = []
        self.reclock = threading.Lock()
        self.initialized = False
        self.destroy = False
        
        #this is for reconnection
        self.name = None
        
    def send(self, msg):
        self.sndlock.acquire()
        self.snd += makeMessage(msg)
        self.sndlock.release()
        
    def run(self):
        while 1:
            time.sleep(0.03)
            while not self.initialized:
                if self.destroy: break
                try:
                    self.init()
                except:
                    print "Connection initialization failed... Will try again."
                    time.sleep(2)
                    
                    
            self.sndlock.acquire()
            sndmsg = self.snd
            self.snd = ''
            self.sndlock.release()
            self.conn.sndlock.acquire()
            self.conn.snd += sndmsg
            self.conn.sndlock.release()
            
            #BAD THIS IS TOO SLOW I THINK
            self.conn.reclock.acquire()
            recmsg = self.conn.rec
            self.conn.rec = []
            self.conn.reclock.release()
            self.reclock.acquire()
            if len(recmsg)  > 0:
                self.rec.extend(recmsg)
            self.reclock.release()
            
            #untested!!!!
            if self.conn.destroy:
                self.name = self.conn.name
                self.initialized = False
            
            if self.conn.destroy and not self.conn.isAlive():
                self.conn.join(.03)
                del self.conn
                self.initialized = False
            
            if self.destroy: break
            
    def killMe(self):
        self.destroy = True
        
    def init(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((SERVERIP, 51007))
        self.sock.setblocking(0)
        self.conn = ClntNB(self.sock, self)
        self.conn.start()
        self.initialized = True
        
        if self.name:
            data = dict()
            data['type'] = 'init'
            data['name'] = b.text
            self.send(PEG_mainLoop.mainLoop().entityDict['Network'].makeDefaultMessage(data))
            print b.text, "re identified succesfully as", self
        print "Connected to server successfully"
        
    
class ClntNB(threading.Thread):
    """this class represents a connection that can send and recv.
    Used by both ClntManager and SrvManager
    """
    id = 0
    idlock = threading.Lock()
    def __init__(self,sock, parent):
        threading.Thread.__init__(self)
        print "initializing"
        
        #we give ClntNB a pointer to its parent if in the future we choose to use events for efficiency
        self.p = parent
        self.name = None
        
#===============================================================================
# #        increment id
# #        idlock causing blocking issues
# #        we do not need a lock because two clntNB objs can never be instantiated at the same time.
#        ClntNB.idlock.acquire()
#        self.id = Clnt.id
#        Clnt.id += 1
#        ClntNB.idlock.release()
#===============================================================================

        print sock.getsockname(), " connected"
        #SEND
        self.snd = ''
        self.sndlock = threading.Lock()
        #RECV
        self.rec = []
        self.reclock = threading.Lock()
        self.lastMsg = None
        
        
        #set to non-blocking mode
        sock.setblocking(0)
        self.sock = sock
        self.destroy = False
        
      
    
    def __del__(self):
        self.destroy = True
        #more important that they knew we DCed then closing cleanly
        #try:self.sock.close()
        #except: pass
        #may cause slow downs so we wont use this
        try: self.join(1)
        except: print "ClntNB destruction error -- can ignore"
        
    def getMsg(self):
        msg = self.sock.recv(6)
        
        if msg == '':
            print self.sock.getsockname(), " destroyed!"
            self.destroy = True

        return self.sock.recv(int(msg))
               
    def run(self):
        while 1:
            time.sleep(0.03)
            #if there is data to send
            if self.snd != '':
                self.sndlock.acquire()
                msg = self.snd
                self.snd = ''
                self.sndlock.release()
                #print "trying to send message"
                try: self.sock.send(msg)
                except:
                    self.destroy = True
                    print "send failed, destroying socket, attempting reconnect"
                
            try:
                while 1:
                    msg = self.getMsg()
                    self.reclock.acquire()
                    self.rec.append( (msg, self) )
                    self.reclock.release()
                    self.lastMsg = pygame.time.get_ticks()
            except:
                pass
            
#===============================================================================
#            if self.lastMsg == None: self.lastMsg = pygame.time.get_ticks()
#            else:
#                if pygame.time.get_ticks()-self.lastMsg > 1000:
#                    self.destroy = True
#===============================================================================
            
            if self.destroy: break
        #when we are done, close the connection
        self.sock.close()
        
