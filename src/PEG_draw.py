import pygame
import math
import PEG_datatypes
import random

#point functions

#offset is offset in radians
def getNgon(sides, radius = 1, center = PEG_datatypes.Vector2d(0,0), offset = 0):
    points = []
    for x in range(int(sides)):
        points.append( ((radius*math.cos(math.pi*2*x/sides + offset)+center.x), (radius*math.sin(math.pi*2*x/sides + offset)+center.y)) )
    return points

def getNgonV2d(sides, radius = 1, center = PEG_datatypes.Vector2d(0,0), offset = 0):
    points = []
    for x in range(sides):
        points.append( PEG_datatypes.Vector2d((radius*math.cos(math.pi*2*x/sides + offset)+center.x), (radius*math.sin(math.pi*2*x/sides + offset)+center.y)) )
    return points

def getGaussianGon(rmin, rmax, amin, amax):
    points = []
    angle = 0
    #TODO: python 2.5 does not have random.triangular!!!!!!!!
    while angle < 2*math.pi - amax:
        r = rmin + random.random()*(rmax-rmin)
        angle += amin + random.random()*(amax-amin)
        x = r*math.cos(angle)
        y = r*math.sin(angle)
        points.append( PEG_datatypes.Vector2d(x,y) )
    return points
#works but sucks
def getLBolt(p1, p2):
    """returns a list of points that make a squiggly line from p1 to p2
    
    p1: Vector2d
    p2: Vector2d"""
    points = []
    points.append(p1)
    
    while 1:
        p = None
        while 1:
            p = points[len(points)-1] + PEG_datatypes.getRandVector2d(random.randint(0,20))
            if (p-p2).magnitude() <= (points[len(points)-1]-p2).magnitude():
                break 
        points.append(p)
        if points[len(points)-1].magnitude() < 5:
            points.append(p2)
            break
        
    r = []
    for p in points:
        r.append(p.getIntTuple())
        
    return r

#draw functions
#def drawGlowingCircle(pos, radius, c1,c2):