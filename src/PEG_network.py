import pygame
from PEG_datatypes import *
import PEG_mainLoop
import PEG_camera
import xml.dom.minidom
import PEG_helpers
from PEG_constants import *
from PEG_entity import *
import PEG_entity
import pedo_lookup
import PEG_server
import PEG_sound
import PEG_effects
import os, sys, traceback

class Network(PEG_entity.Entity):
    
    def __init__(self, exml = None):
        PEG_entity.Entity.__init__(self, Rect2d(0,0,0,0))
        
         #server mode or client mode
        self.server = False
        if exml and exml.hasAttribute("server"):
            if exml.getAttribute("server") == "True":
                self.server = True   
        #set up the server yo
        if self.server: 
            self.mgr = PEG_server.ClntManager()
            self.mgr.start()
        else: self.mgr = PEG_server.SrvManager()
        
        self.lastTimeWeGrabbedData = 0      #used for testing network speed, not important
        
        
        #GAME VARIABLES
        
        self.enDict = dict()        #contains all info about networked game entities
        self.destroyList = list()
        self.selection = None       #selection (with mouse) for additional informaiton display
        self.gameTime = "00:00"     #game time, either formatted and sent (server) or received and displayed (client)
        self.color = ''             #only relevent for client, set by SelectEn, represents what shape (color) in enDict is the player
        
        self.state = "waiting"                              #game state, they are ('waiting','starting','started','end') and repeats
        self.stateStartTime = pygame.time.get_ticks()       #when we started the last state, used by server

        self.sendFlag = True
        
    def __del__(self):
        try:
            self.mgr.killMe()
        except: print "network closing error -- can ignore this"
        
    def update(self):
        print 1
        self.grabData()
        #process coordinates
        
        print 2
        if self.server:
            self.logic()
            
        print 3
        #TEMPORARY FOR ON SCREEN CONTROLLED VERISON
        for e in self.enDict.values():
            if hasattr(e,'controlled') and  e.controlled:
                e.update()
               
        print 4 
        self.deleteEntities()
        
        
        print 5
        if not self.server:
            self.mgr.update()
            
        print 6
        #send out the data
        if self.server and self.sendFlag: 
            self.sendData()
            #self.sendFlag = False
        else: self.sendFlag = True
        
    def grabData(self):
        #get data, interpret it, and create objects as needed
        #get all the messages
        recv = None
        try:
            self.mgr.reclock.acquire()
            recv = self.mgr.rec
            self.mgr.rec = ['',]
            self.mgr.reclock.release()
        except: 
            print "data grab exception"
            try: self.mgr.reclock.release()
            except: print "reclock release exception"
        
        #read in data
        #Messages are in this form:
        #[0:letter representing message type][5:representing object id][21:object name][30:x][39:y][49:state]
        if len(recv) > 0:
            for m in recv:
                if m:
                    #self.processNetMessage(m)
                    try:
                        pass
                        self.processNetMessage(m)
                    except: 
                        print "data misformated: ", m

    def changeState(self,state):
        self.state = state
        self.stateStartTime = pygame.time.get_ticks()
        
        data = dict()
        if state == "starting":
            data['type'] = "state"
            data['state'] = "starting"
        elif state == "end":
            #determine the winner
            #adding sides
            def isTeamA(en): return en.getTeam == "A"
            def isTeamB(en): return en.getTeam == "B"
            sumA = sumB = 0
            for e in self.enDict.values():
                if e.team == "A":
                    sumA += e.sides
                elif e.team == "B":
                    sumB += e.sides           
            winner = "A"
            if sumB > sumA: winner = "B"
            if sumB == sumA: winner = "TIE"
            data['type'] = "state"
            data['state'] = "gameover"
            data['winner'] = winner
            data['Ateam'] = sumA
            data['Bteam'] = sumB
        if len(data) > 0:
            print data
            self.send([data,])
            
        print "game state changed to: ", state
        
    def logic(self):
        if self.state == "waiting":
            self.gameTime = ""
            #TODO if all players identified AND power source located then change to state = started
            return
        if self.state == "starting":
            timeDiff = pygame.time.get_ticks() - self.stateStartTime
            if timeDiff > 10000:
                self.changeState("started")
                return
            try: self.gameTime = 10 - timeDiff / 1000 
            except: self.gameTime = 10
            return
        if self.state == "started":
            timeDiff = pygame.time.get_ticks() - self.stateStartTime
            if timeDiff > GAME_PLAY_TIME:
                self.changeState("end")
                return
            
            #figure out the time
            untilEnd = (GAME_PLAY_TIME - timeDiff)/1000
            minutes = int(untilEnd/60)
            seconds = untilEnd - minutes*60
            self.gameTime = str(minutes)+':'+str(seconds)
            
            
            self.checkTraps()
            #revive players
            self.revivePlayers()
            return
        
        if self.state == "end":
            timeDiff = pygame.time.get_ticks() - self.stateStartTime
            if timeDiff > 10000:
                #TODO if no clietns have dropped
                self.changeState("starting")
                #TODO delete all non shape entities
                for e in self.enDict:
                    if isinstance(e,PEG_entity.ShapeEn):
                        e.sides = 3
                
            self.gameTime = "00:00"        
            return
            
    
    def checkTraps(self):
        for e in self.getShapeEnList():
            for f in self.getBuildingEnList():
                if f.sides == 3 and (e.pos.getPosition() - f.pos.getPosition()).magnitude() < TRAP_RADIUS:
                    print "trap triggered"
                    f.sides -= 1
                    e.attackedBy(f)
                
    #TODO move revive players into processArduino (so it happens when you do some revive action
    def revivePlayers(self):
        for e in self.enDict.values():
            if e.sides == 0:
                #TODO check distance from power source --> revive
                #do we give zero resources?
                if (e.pos.getPosition() - self.enDict['ThePSource'].pos.getPosition()).magnitude() < POWERSOURCE_ACTIVE_RADIUS:
                    e.sides = 3 
                    e.state = "alive"
                    print e.name, "revived"
                    #animation
                    args = [8, e.pos.getPosition(), e.color, ACTION_RADIUS]
                    self.send([(PEG_effects.quickData(*args)),])
        
    def processNetMessage(self, data):
        if data['type'] == "position":
            #We only update entities after one has been idnetififed
            if self.server or self.color:
                self.parseEnData(data)          #has server specific routine inside
        if data['type'] == "time":
            self.gameTime = data['time']
        if not self.server and data['type'] == "effect":        #client specific
            #should have position, id, etc 
            print "got effect from serva", data
            PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect(PEG_effects.quickEffect(
                                                                                               int(data['id']), 
                                                                                               Vector2d(int(data['x']),int(data['y'])),
                                                                                               (int(data['r']),int(data['g']),int(data['b'])), 
                                                                                               int(data['radius']),
                                                                                               int(data['sides'])
                                                                                               ))
        if not self.server and data['type'] == "state":        #client specific
            menu = PEG_mainLoop.mainLoop().entityDict["MenuEn"]
            if data['state'] == "gameover":
                menu.gameover = True
                menu.winner = data['winner']
                menu.Ascore = data['Ateam']
                menu.Bscore = data['Bteam']
            elif data['state'] == "starting":
                menu.gameover = False
                
        if self.server and 'relay' in data:                     #server specific
                del data['relay']
                self.send(self.makeDefaultMessage(data))
                #print "relayed", data
        
        if self.server and data['type'] == "event":             #server specific
            if data['event'] == "arduino":
                if 'ThePSource' in self.enDict:
                    self.processArduinoEvent(data)
                
    def processArduinoEvent(self,data):
        #try:
        print "got", data['arduino'], "from", data['name']
        #whirly bird for attacking
        if data['arduino'] == "sweep":
            for e in [item for item in self.enDict.values() if hasattr(item,"attackedBy")]:
                if self.enDict[data['name']].team != e.team:
                    if (self.enDict[data['name']].pos.getPosition() - e.pos.getPosition()).magnitude() < ACTION_RADIUS:
                        print self.enDict[data['name']].name, "attacks", e.name, "successfully"
                        e.attackedBy(self.enDict[data['name']])
                        #play effect
                        args = [7, e.pos.getPosition(), pedo_lookup.colorMap[data['name']], ACTION_RADIUS]
                        #PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect(PEG_effects.quickEffect(*args))
                        self.send([PEG_effects.quickData(*args),])
                        
        #tomahawk for resources and construction
        if data['arduino'] == "tomahawk":
            
            flag1 = True
            for e in self.getBuildingEnList():
                if e.sides >= 5:
                    if (self.enDict[data['name']].pos.getPosition() - e.pos.getPosition()).magnitude() < POLYFACTORY_ACTIVE_RADIUS:
                        if self.enDict[data['name']].resources == self.enDict[data['name']].sides:
                            print self.enDict[data['name']].name, "has POLYFACTORIZED!!!!"
                            args = [9, self.enDict[data['name']].pos.getPosition(), self.enDict[data['name']].color, ACTION_RADIUS,self.enDict[data['name']].sides+1]
                            self.send([PEG_effects.quickData(*args),])
                            self.enDict[data['name']].sides += 1
                            self.enDict[data['name']].resources = 0
                            flag1 = False
                            break
            if flag1 and (self.enDict[data['name']].pos.getPosition() - self.enDict['ThePSource'].pos.getPosition()).magnitude() < POWERSOURCE_ACTIVE_RADIUS:
                print self.enDict[data['name']].name, "gathers 1 resource"
                self.enDict[data['name']].addResource()
                args = [5, self.enDict[data['name']].pos.getPosition(), self.enDict[data['name']].color, ACTION_RADIUS]
                sData = PEG_effects.quickData(*args)
                print  "in respones to arduino event, sending:", sData
                self.send([sData,])
            elif flag1 and self.enDict[data['name']].resources > 0:  #TODO change to 0, keep this way for testing
                self.enDict[data['name']].resources -= 1
                flag2 = True
                print self.enDict.values()
                for e in self.getBuildingEnList():
                    if (self.enDict[data['name']].pos.getPosition() - e.pos.getPosition()).magnitude() < BUILDING_RADIUS:
                        if self.enDict[data['name']].team == e.team:
                            print e.name, "upgraded by", data['name']
                            e.addSide()
                            flag2 = False
                            args = [9, e.pos.getPosition(), e.color, ACTION_RADIUS,e.sides+1]
                            self.send([PEG_effects.quickData(*args),])
                            break
                #no upgrade, then make new structure
                if flag2:
                    print self.enDict[data['name']], "created new building"
                    sdata = dict()
                    sdata['type'] = "position"
                    sdata['entity'] = "BuildingEn"
                    sdata['x'] = self.enDict[data['name']].pos.x
                    sdata['y'] = self.enDict[data['name']].pos.y
                    #TODO make some system for creating unique names
                    sdata['name'] = random.randint(0,5000) 
                    sdata['team'] = self.enDict[data['name']].team
                    sdata['sides'] = 1
                    self.processNetMessage(sdata)
                    #bad but w/e
                    self.enDict[sdata['name']].team = sdata['team']
                    args = [9, self.enDict[data['name']].pos.getPosition(), pedo_lookup.colorMap[sdata['team']], 3]
                    self.send([PEG_effects.quickData(*args),])
        #TODO hit explosion thing I dunno
        #play some effect for now?
        if data['arduino'] == "whirlybird":
            #TODO detect mode????
            #for now everything is visible
            pass
        #except: print "arduino event failed"
        
    def sendData(self):
        #TODO SOMETHING IS WEONG HERE DELETES IMPORTANT MESSAGES
        #THIS IS BAD, may cause some important one shot message (like game state change) to never get sent)
        self.mgr.clearSend()
        datalist = list()
        data = dict()
        data['type'] = 'time'
        data['time'] = str(self.gameTime)
        datalist.append(data)
        for e in self.enDict.values():
            if hasattr(e, 'updateData'):
                e.updateData()
            datalist.append(e.data)
        self.mgr.send(datalist)       
        
    def parseEnData(self,d):
        if d['name'] not in self.enDict:
            try:
                exml = xml.dom.minidom.parseString("<entity type=\"" + d['entity']
                                                   + "\" x=\"" + str(d['x'])
                                                   + "\" y=\"" + str(d['y'])
                                                   + "\" name=\"" + str(d['name'])
                                                   + "\" size=\"3"
                                                   +"\" />").getElementsByTagName("entity")[0]
                                                   
                self.enDict[d['name']] = pedo_lookup.enTables(exml)
                print d['entity'], ": ", d['name'], " created!"
                
#===============================================================================
#                try:
#                    if 'team' in data:
#                        self.enDict[d['name']].team = data['team']
#                except: pass
#===============================================================================
                
                #USER CONTROLLED STUFF
                if self.color == d['name']:
                    self.enDict[d['name']].controlled = True
                    print "self identified as: ", d['name']
            except: 
                    print "data misformated: ", d
                    exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
                    print "*** print_tb:"
                    traceback.print_tb(exceptionTraceback, limit=1, file=sys.stdout)
                    print "*** print_exception:"
                    traceback.print_exception(exceptionType, exceptionValue, exceptionTraceback,
                                              limit=2, file=sys.stdout)
                
        
        try:
            self.enDict[d['name']].data = d
        except: print d['name']," data set failed"
        try:            
            self.enDict[d['name']].updatePos()
        except: print d['name'], "update pos failed"
    
    def send(self,data):
        #TODO switch to mustsend
        #used by SERVER only
        self.mgr.mustSend(data)
    
    def getShapeEnList(self):
        shapeList = list()
        for e in self.enDict.values():
            if isinstance(e,PEG_entity.ShapeEn):
                shapeList.append(e)
        #print len(shapeList)
        return shapeList
    
    def getBuildingEnList(self):
        shapeList = list()
        for e in self.enDict.values():
            if isinstance(e,PEG_entity.BuildingEn):
                shapeList.append(e)
        #print len(shapeList)
        return shapeList

    def deleteEntities(self):
        for e in self.enDict.values():
            if e.state == "destroy":
                self.destroyList.append(e)
        
        for s in self.destroyList:
            try: del self.enDict[s]
            except: print "delete entity failed"
        self.destroyList = list()
        
    def draw(self):
        for e in self.enDict.itervalues():
            e.draw()