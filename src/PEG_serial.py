import pygame
import serial
import random
import threading
import copy
import xml.dom.minidom
import PEG_entity
import time
import copy
import PEG_mainLoop
from PEG_datatypes import *
from PEG_constants import *



#need a way to manage multiple events within a time frame. Can do this by setting special expirations that when expire will roll back to last state.
#for example U + L player hits U, watcher registers U starts 1 frame countdown wait for L, if L then continues as usual, if not, then rolls back to original state
#that's nasty.
class IMan:
    def __init__(self):
        self.watcherList = list()
        self.watcherAddList = list()
        self.destroyList = list()
        self.eventList = list()
        self.eventListLock = threading.Lock()
        pass
    
    def addWatcherAdder(self, watcher):
        self.watcherAddList.append(watcher)
        
    def push_event(self,event):
        self.eventListLock.acquire()
        self.eventList.append(event)
        self.eventListLock.release()
    
    def update(self):
        
        #UNTESTED
        self.eventListLock.acquire()
        eventList = list(self.eventList)
        self.eventList = list()
        self.eventListLock.release()
        
        for e in eventList:
            self.add_watchers(e)
            self.filter_event(e)
        
                    
        self.update_watchers()
        self.destroy_watchers()
       
    def filter_event(self, event):
        for w in self.watcherList:
            w.push_event(event)
    
    def add_watchers(self, event):
        for e in self.watcherAddList:
            if e.addMe(event):
                self.watcherList.append(copy.deepcopy(e))
    
    def update_watchers(self):
        #checks for additional watcher state change conditions
        for e in self.watcherList:
            e.update()
        #sees if any watcers need to be destroyed or triggered
        for e in self.watcherList:
            if e.trigger:
                pass
                #print e.trigger
        for e in self.watcherList:
            if e.destroy:
                self.destroyList.append(e)
      
    def destroy_watchers(self):
        for e in self.destroyList:
            #print "destroyed: ", e
            self.watcherList.remove(e)
        self.destroyList = list()
        pass
    

    
class IManWatcher:
    def __init__(self):
        self.destroy = False
        self.trigger = ''
        self.type = "base"
        self.expiration = 1000
        
    def push_event(self):
        pass
    
    def update(self):
        time = pygame.time.get_ticks()
        if time - self.lastUpdate > self.expiration:
            self.destroy = True
            
        
    def addMe(self, event):
        #returns true or false"
        pass
    
    def isDestroyed(self):
        return self.destroy
    
def makeCircular(name,dir="left"):
    topxml = xml.dom.minidom.parse("inputs2.xml")
    watcherxml = None
    for n in topxml.getElementsByTagName('input'):
        if n.hasAttribute("name") and n.getAttribute("name") == name:
            watcherxml = n
            break
    return IManWatcherCircular(watcherxml,dir)

class IManWatcherCircular(IManWatcher):
    def __init__(self, xml, dir = "left"):
        IManWatcher.__init__(self)
        self.type = "whirlybird"
        self.dir = dir
        self.activePos = None
        self.startPos = None
        
        
        
        self.posList = list()
        #WORKING
        for n in xml.getElementsByTagName('event'):
            if n.hasAttribute('key'):
                self.posList.append(n.getAttribute('key'))
        #print self.posList   
        #self.posList = ['1','2','3','6','9','8','7','4']
        
    def addMe(self,event):
        for e in self.posList:
            if e == event:
                self.activePos = self.posList.index(e)
                self.startPos = self.activePos 
                return True
        return False
    def push_event(self,event):
        if self.dir == "left": c = -1
        else: c = 1
        
        if event == self.posList[self.activePos]:
            #print "advanced to ", self.activePos
            self.activePos = (self.activePos + c)%len(self.posList)
            self.lastUpdate = pygame.time.get_ticks()
            if self.activePos == self.startPos:
                print "triggered whirlybird", self.dir
                self.destroy = True
                self.trigger = "whirlybird"
                #do shit
        pass


def makeGeneric(name):
    topxml = xml.dom.minidom.parse("inputs2.xml")
    watcherxml = None
    for n in topxml.getElementsByTagName('input'):
        if n.hasAttribute("name") and n.getAttribute("name") == name:
            watcherxml = n
            break
    return IManWatcherGeneric(watcherxml)
        
#basic watcher, checks for static input combinations
#works pretty great so far. Branches are pretty nice. To make it even more cool, let inputs branch out tree style.
class IManWatcherGeneric(IManWatcher):
    def __init__(self,xml):
        IManWatcher.__init__(self)
        try: self.type = xml.getAttribute('type')
        except: self.type = "base"
        
        self.triggerList = list()
        self.trigger = ''
        self.activeBranch = None
        self.activePos = None
        self.lastUpdate = 0
        self.expiration = 1000 #in ms
        
        #WORKING
        for m in xml.getElementsByTagName('branch'):
            branch = list()
            for n in m.getElementsByTagName('event'):
                if n.hasAttribute('key'):
                    branch.append(n.getAttribute('key'))
            self.triggerList.append(branch)
        
        #print self.triggerList
        
    def addMe(self,event):
        #for strings
        
        #sets values for itself, caller must make deep copy of this instance as an event
        for i in range(len(self.triggerList)):
            if self.triggerList[i][0] == event:
                self.activeBranch = i
                self.activePos = 0
                self.lastUpdate = 99999999999999999999999
                return True
        return False
    
    def push_event(self,event):
        #pointless
        #self.trigger = ''
        
        #events are strings
        #if event is correct, advance
        if self.triggerList[self.activeBranch][self.activePos] == event:
            print "triggered ", self.activePos
            self.activePos += 1
            self.lastUpdate = pygame.time.get_ticks()
            if self.activePos == len(self.triggerList[self.activeBranch]):
                self.destroy = True
                #TODO change this to name found in xml
                self.trigger = 'generic'
            
            #pointless code
            #try: self.activePos = (self.activePos+1)%len(self.triggerList[self.activeBranch] - 1)
            #except: self.activePos = 0
    
    
class SerialEntity(PEG_entity.Entity):
    def __init__(self, exml = None):
        PEG_entity.Entity.__init__(self, Rect2d(0,0,0,0))
#===============================================================================
#        self.ser = None
#===============================================================================
        try:
            self.ser = SerialMan()
        except: self.ser = None
#===============================================================================
#        self.init = False
#===============================================================================
        self.lastInputList = list()
        
        self.curAction = ''
        self.curActionStart = 0
        
    def __del__(self):
        pass
#===============================================================================
#        try: self.ser.join(1)
#        except: print "failed to close serial port", self.ser
#===============================================================================
        
    def update(self):
        try:
            self.parse_event( self.ser.getMessage() )
        except: pass
#===============================================================================
#        if not self.init:
#            try:
#                self.ser = SerialThread()
#                self.ser.start()
#                self.init = True
#            except: pass
#===============================================================================
#===============================================================================
#        if self.init:
#            if self.ser.v:
#                self.ser.vlock.acquire()
#                #print self.ser.v
#                self.parse_event(self.ser.v)
#                self.ser.v = ''
#                self.ser.vlock.release()
#                
#                self.getActionString()
#===============================================================================
                
            
    
    def getActionString(self):
        action = self.getAction()
        if action != '':
            if action != self.curAction:
                self.curAction = action
                self.curActionStart = pygame.time.get_ticks()
                print "starting", action
                return "starting " + action
            elif pygame.time.get_ticks() - self.curActionStart > 2000:
                self.curAction = ''
                #print "triggered", action
                return "triggered " + action
            return None
        self.curAction = ''
        return 'dead'  
        
    
    def getAction(self):
        def getx(ls):
            return ls[0][0]
        def gety(ls):
            return ls[0][1]
        def listdiff(ls):
            sum = 0
            for i in range(len(ls)-1):
                sum += math.fabs(ls[i]-ls[i+1])
            return sum
        lx = map(getx, self.lastInputList)
        ly = map(gety, self.lastInputList)
        dx = listdiff(lx)
        dy = listdiff(ly)
        if dx > 150 and dy > 150 and math.fabs(dy - dx) < 125:
            pass
            #print "whirlybird"
            return "whirlybird"
        elif dx - dy > 125:
            pass
            #print "sweep"
            return "sweep"
        elif dx - dy < -125:
            pass
            #print "tomahawk"
            return "tomahawk"
        return ''
        
        
        
    
    def flushInputList(self):
        while len(self.lastInputList) > 0 and pygame.time.get_ticks()-self.lastInputList[0][1] > MAX_SERIAL_INPUT_TIME_LENGTH:
            self.lastInputList.pop(0)
    
    def parse_event(self,event):
        try:
            data = map(int,event.split(" "))
            if len( data ) == 4:
                self.flushInputList()
                self.lastInputList.append((data,pygame.time.get_ticks()))
                
                
        except: "parse serial data failed"
        
#===============================================================================
# class SerialEntity(PEG_entity.Entity):
#    def __init__(self, exml = None):
#        PEG_entity.Entity.__init__(self, Rect2d(0,0,0,0))
#        self.ser = None
# #===============================================================================
# #        self.man = IMan()
# #        self.man.addWatcherAdder(makeCircular("tomahawk","left"))
# #        self.man.addWatcherAdder(makeCircular("tomahawk","right"))
# #===============================================================================
#        self.init = False
#        
#        self.lastInputList = list()
#        self.messyStartTime = 0
#    def __del__(self):
#        try: self.ser.join(1)
#        except: print "failed to close serial port", self.ser
#        
#    def update(self):
#        if not self.init:
#            try:
#                self.ser = SerialThread()
#                self.ser.start()
#                self.init = True
#            except: pass
#        
#        if self.init:
#             if self.ser.v:
#                self.ser.vlock.acquire()
#                #self.man.push_event(self.ser.v)
#                #self.push_event(self.ser.v)
#                self.parse_event(self.ser.v)
#                #print self.ser.v
#                self.ser.v = ''
#                self.ser.vlock.release()
#                
#        #self.man.update()
#        #self.getMotion()
# #===============================================================================
# #        if self.getMessyness() > 3:
# #            if self.messyStartTime == 0:
# #                self.messyStartTime = pygame.time.get_ticks()
# #            elif pygame.time.get_ticks() - self.messyStartTime > 3000:
# #                network = PEG_mainLoop.mainLoop().entityDict['Network']
# #                data = dict()
# #                data['type'] = "event"
# #                data['event'] = "arduino"
# #                data['name'] = network.color
# #                network.mgr.send( network.makeDefaultMessage(data) )
# #                print "msg sent"
# #                
# #                #TODO send stuff
# #        else:
# #            self.messyStartTime = 0
# #===============================================================================
#        
#    
#    def flushInputList(self):
#        while len(self.lastInputList) > 0 and pygame.time.get_ticks()-self.lastInputList[0][1] > MAX_SERIAL_INPUT_TIME_LENGTH:
#            self.lastInputList.pop(0)
#            
#    def getMessyness(self):
#        self.flushInputList()
#        dirSet = set()
#        for e in self.lastInputList:
#            dirSet.add(e[0])
#        print dirSet
#        #print "we have", len(dirSet)
#    
#    def getMotion(self):
#        self.flushInputList()
#        def getDirFilter(s):
#            def filterChar(c):
#                if c[0] == s:
#                    return True
#                return False
#            return  filterChar
#        print len(filter(getDirFilter('1'),self.lastInputList)), len(filter(getDirFilter('4'),self.lastInputList)), len(filter(getDirFilter('7'),self.lastInputList))
#        if len(filter(getDirFilter('1'),self.lastInputList)) > 2 and len(filter(getDirFilter('4'),self.lastInputList)) > 0 and len(filter(getDirFilter('7'),self.lastInputList)) > 0:
#            print "tomahawk"
#        
#        
#    #quick and dirty way to get inputs
#    def push_event(self,event):
#        self.flushInputList()
#        self.lastInputList.append((event,pygame.time.get_ticks()))
#    
#    def parse_event(self,event):
#        data = map(int,event.split(" "))
#        print data
#===============================================================================

class SerialMan:
    def __init__(self):
        try: 
            self.ser = serial.Serial("/dev/ttyUSB0", 9600)
            print "serial initialized", self.ser
        except: self.ser = None
        
        if self.ser == None:
            #scan for first serial port
            for i in range(256):
                try:
                    self.ser = serial.Serial(i, 9600)
                    print "serial initialized: ", self.ser
                    break
                except:
                    self.ser = None
            if not self.ser:
                print "serial could not be initialized"
                raise Exception("serial failed")    
    def getMessage(self):
        #this is bad, will block if serial is printing slower than framerate
        #flush bad data
        msg = self.ser.readline()
        msg = self.ser.readline()
        self.ser.flushInput()
        return msg
    
class SerialThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.v = ''
        self.vlock = threading.Lock()
        self.msg = ''
        self.init = True
        
        try: 
            self.ser = serial.Serial("/dev/ttyUSB0", 9600)
            print "serial initialized", self.ser
        except: self.ser = None
        
        if self.ser == None:
            #scan for first serial port
            for i in range(256):
                try:
                    self.ser = serial.Serial(i, 9600)
                    print "serial initialized: ", self.ser
                    break
                except:
                    self.ser = None
            if not self.ser:
                self.init = False
                print "serial intializing totally failed"
        
        
    def __del__(self):
        pass
    
    def run(self):
        while 1 and self.init:
            msg = self.ser.readline()
            #print "read", msg
            if str(msg).strip():
                self.vlock.acquire()
                self.v = msg
                self.vlock.release()
            msg = ""
            
    def runo(self):
        while 1:
            while 1:
                msg = self.ser.read()
                if msg == '\n':
                    break
            while 1:
                msg = self.ser.read()
                if msg == '\r':
                    if self.msg != ' ':
                        self.vlock.acquire()
                        self.v = self.msg
                        self.vlock.release()
                    self.msg = ''
                    #time.sleep(.005)
                    #self.ser.flushInput()
                    break
                elif msg != '\n':
                    self.msg += msg
                

    