import PEG_entity
import pedo_player
import PEG_serial
import PEG_network
import pygame
import PEG_effects
import os
from PEG_constants import *

enList = ("Player", "SolidEntity")
def enTables(exml):
    if not exml.hasAttribute("type"):
        print "nothing found"
        return None
    
    name = exml.getAttribute("type")
    if name == "Player":
        return pedo_player.Player(exml)
    if name == "SolidEntity":
        return PEG_entity.SolidEntity(exml)
    if name == "Editor":
        return PEG_entity.Editor(exml)
    if name == "Network":
        return PEG_network.Network(exml)
    if name == "ShapeEn":
        return PEG_entity.ShapeEn(exml)
    if name == "MenuEn":
        return PEG_entity.MenuEn(exml)
    if name == "EffectMenu":
        return PEG_effects.EffectMenu(exml)
    if name == "SerialEntity":
        return PEG_serial.SerialEntity(exml)
    if name == "PowerSourceEn":
        return PEG_entity.PowerSourceEn(exml)
    if name == "BuildingEn":
        return PEG_entity.BuildingEn(exml)
    
soundList = (
             os.path.join('data','sfx01.wav'),
             )

colorMap = dict()
colorMap["RED"] = (255,0,0)
colorMap["CYAN"] = (0,255,255)
colorMap["YELLOW"] = (255,255,0)

colorMap["BLUE"] = (0,0,255)
colorMap["PURPLE"] = (255,0,255)
colorMap["ORANGE"] = (255,127,0)

colorMap["player0"] = (255,0,0)
colorMap["player1"] = (0,255,0)

colorMap["A"] = COLOR_RED
colorMap["B"] = COLOR_BLUE
colorMap["neutral"] = COLOR_WHITE

teamMap = dict()
teamMap['neutral'] = "neutral"
teamMap["RED"] = "A"
teamMap["CYAN"] = "A"
teamMap["YELLOW"] = "A"

teamMap["BLUE"] = "B"
teamMap["PURPLE"] = "B"
teamMap["ORANGE"] = "B"



fontList = (
            os.path.join('data','Alien-Encounters-Regular.ttf'),
            os.path.join('data','romance_fatal_lcd.ttf'),
            os.path.join('data','DS-DIGI.ttf'),
            os.path.join('data','DS-DIGI.ttf'),
            os.path.join('data','digital-7.ttf'),
            )