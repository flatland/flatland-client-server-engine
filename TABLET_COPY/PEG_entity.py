import pygame
from PEG_datatypes import *
import PEG_mainLoop
import PEG_camera
import xml.dom.minidom
import PEG_helpers
from PEG_constants import *


class Entity:
    def __init__(self, position = Rect2d(0,0,0,0)):
        """initialize Entity at position position
        
        position: Vector2d"""
        self.pos = position
        self.vel = Vector2d(0,0)
        
    def teleport(self, position):
        """moves entity to position
        
        position: PEG_datatypes.Vector2d"""
        self.pos.x = position.x
        self.pos.y = position.y
        
    def draw(self):
        pass
    
    def update(self):
        pass
    
    def getxml(self):
        return None
    
import pedo_lookup
    
class MovingEntity(Entity):
    def __init__(self, position = Rect2d(0,0,0,0)):
        Entity.__init__(self,position)
    def collide(self,e):
        """is called when there is a colision with Entity e
        
        e: Entity that is collided with"""
        pass
        
class StaticEntity(Entity):
    def __init__(self, position = Rect2d(0,0,0,0)):
        Entity.__init__(self,position)

class SolidEntity(StaticEntity):
    def __init__(self, exml):
        """loads a static terrain square
        
        img: string filename of image"""
        tr = Rect2d(0,0,0,0)
        if exml.hasAttribute("x"):
            tr.x = float(exml.getAttribute("x"))
        if exml.hasAttribute("y"):
            tr.y = float(exml.getAttribute("y"))
        if exml.hasAttribute("w"):
            tr.w = float(exml.getAttribute("w"))
        if exml.hasAttribute("h"):
            tr.h = float(exml.getAttribute("h"))
        if exml.hasAttribute("img"):
            self.mainSurface = pygame.image.load(exml.getAttribute("img")).convert()
            self.mainSurface.set_colorkey(COLOR_GREEN,pygame.RLEACCEL)
        
        self.myxml = exml
        StaticEntity.__init__(self,tr)
        
    def draw(self):
        PEG_mainLoop.mainLoop().cam.drawOnScreen(self.mainSurface, self.pos)
    
    def getxml(self):
        self.myxml.setAttribute("x", str(self.pos.x))
        self.myxml.setAttribute("y", str(self.pos.y)) 
        return self.myxml
    
    
import PEG_visext
import pedo_lookup
class SelectMenu(Entity):
    def __init__(self,exml=None):
        """a select menu to choose things...
        
        exml: xml.dom.element"""
        
        #extract number of menu items
        #calculate # of rows and sizes
        #create button surfaces and button down surfaces
        #draw button surfaces onto main surface
        self.destroy = False
        self.buttonList =[
                        PEG_visext.TextButton01(Rect2d(175, 180,150,60), (255,0,0), "RED"),
                        PEG_visext.TextButton01(Rect2d(175, 270,150,60), (0,255,255), "CYAN"),
                        PEG_visext.TextButton01(Rect2d(175, 360,150,60), (255,255,0), "YELLOW"),
                       
                        PEG_visext.TextButton01(Rect2d(450, 180,150,60), (0,0,255), "BLUE"),
                        PEG_visext.TextButton01(Rect2d(450, 270,150,60), (255,0,255), "PURPLE"),
                        PEG_visext.TextButton01(Rect2d(450, 360,150,60), (255,127,0), "ORANGE")]

        Entity.__init__(self,Rect2d(0,0,0,0))
        
    def update(self):
        mousePos = pygame.mouse.get_pos()
        for e in PEG_mainLoop.mainLoop().eventList:
            if e.type == pygame.MOUSEBUTTONUP:
                for b in self.buttonList:
                    if b.isPointInside(mousePos):
                        PEG_mainLoop.mainLoop().entityDict['Network'].color = b.text
                        print "you have chosen to be ", b.text
                        #send message to server
                        data = dict()
                        data['type'] = 'init'
                        data['name'] = b.text
                        network = PEG_mainLoop.mainLoop().entityDict['Network']
                        network.mgr.send( [data,])
                        data['type'] = 'position'
                        data['entity'] = 'ShapeEn'
                        data['x'] = 500
                        data['y'] = 200
                        network.mgr.send( [data,] )
                        self.destroy = True
    def draw(self):
        PEG_speech.Speech().setSize(30, pedo_lookup.fontList[0])
        PEG_speech.Speech().writeCentered(
                                          PEG_mainLoop.mainLoop().screen,
                                          Vector2d(250,150),
                                          "TEAM A",
                                          pedo_lookup.colorMap["A"],
                                          )
        PEG_speech.Speech().setSize(30, pedo_lookup.fontList[0])
        PEG_speech.Speech().writeCentered(
                                          PEG_mainLoop.mainLoop().screen,
                                          Vector2d(525,150),
                                          "TEAM B",
                                          pedo_lookup.colorMap["B"],
                                          )
        PEG_speech.Speech().setSize(45, pedo_lookup.fontList[0])
        PEG_speech.Speech().writeCentered(
                                          PEG_mainLoop.mainLoop().screen,
                                          Vector2d(387,80),
                                          "SELECT YOUR COLOR",
                                          FL_SELECT_YOUR_COLOR_COLOR,
                                          )
        for b in self.buttonList:
            b.drawAbs()
        pass

import PEG_draw
import PEG_speech
class MenuEn(Entity):
    def __init__(self, exml = None):
        Entity.__init__(self,Rect2d(640,0,160,480))
        self.scan = False
        
        #TODO if network is server, self.selectmode = false
        self.selectMode = True
        self.select = SelectMenu()
        self.fieldFake = pygame.image.load("eggleston_tiled.png").convert()
        
        #gameover screen display info stuff
        self.gameover = False
        self.winner = ""
        self.Ascore = ""
        self.Bscore = ""

    def update(self):
        #grab input
        mousePos = pygame.mouse.get_pos()
        for e in PEG_mainLoop.mainLoop().eventList:
            if e.type == pygame.MOUSEBUTTONUP:
                if PEG_helpers.livesIn(mousePos,Rect2d(self.pos.x+20,80,120,120)):
                    network = PEG_mainLoop.mainLoop().entityDict['Network']
#===============================================================================
#                    #TODO figure out why I have this here???
#                    for s in network.enDict.keys():
#                        network.destroyList.append(s)
#                    self.scan = not self.scan
#                    print "scan mode: ", self.scan
#===============================================================================
#===============================================================================
#                    PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect(PEG_effects.quickEffect(3, Vector2d(300,200), COLOR_CHARTREUSE, 150))
#                    PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect(PEG_effects.quickEffect(2, Vector2d(300,200), (255,255,255), 300))
#                    PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect(PEG_effects.quickEffect(6, Vector2d(300,200), (255,255,255), 300))
#                    PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect(PEG_effects.quickEffect(4))
#===============================================================================
#===============================================================================
#                if PEG_helpers.livesIn(mousePos,Rect2d(self.pos.x+20,200,120,120)):
#                    print "box2"
#                if PEG_helpers.livesIn(mousePos,Rect2d(self.pos.x+20,340,120,120)):
#                    print "box3"
#===============================================================================

        if PEG_mainLoop.mainLoop().entityDict['Network'].server:
            mousePos = pygame.mouse.get_pos()
            for e in PEG_mainLoop.mainLoop().eventList:
                if e.type == pygame.MOUSEBUTTONUP:
                    data = dict()
                    data['type'] = "position"
                    data['entity'] = "PowerSourceEn"
                    data['x'] = mousePos[0]
                    data['y'] = mousePos[1]
                    data['name'] = "ThePSource" 
                    data['team'] = "neutral"
                    data['sides'] = "123"
                    network = PEG_mainLoop.mainLoop().entityDict['Network']
                    network.processNetMessage(data)
                    if network.state == "waiting":
                        network.changeState("starting")
                    
                    
            self.selectMode = False
            
        if self.selectMode:
            self.select.update()
            if self.select.destroy:
                self.selectMode = False
                del self.select
                
    def draw(self):
        
        
        
        x = self.pos.x
        screen = PEG_mainLoop.mainLoop().screen

#===============================================================================
#        #we don't need this
#        pygame.draw.rect(screen, FL_COLOR_MENU_BACKGROUND, pygame.Rect(x,-1,162,482), 1)
#===============================================================================

        #text
        #TODO see how much time this takes to write
        PEG_speech.Speech().setSize(12, pedo_lookup.fontList[2])
        PEG_speech.Speech().writeCentered(
                                          screen,
                                          Vector2d(x+140,10),
                                          str(PEG_mainLoop.mainLoop().fps),
                                          FL_COLOR_TIMER
                                          )
        
        PEG_speech.Speech().setSize(50, pedo_lookup.fontList[3])
        PEG_speech.Speech().writeCentered(
                                          screen,
                                          Vector2d(x+80,45),
                                          PEG_mainLoop.mainLoop().entityDict['Network'].gameTime,
                                          FL_COLOR_TIMER
                                          )
        
        
#===============================================================================
#        #square button thing
#        pygame.draw.rect(screen,(255,255,255),pygame.Rect(x+20,80,120,120),1)  
#        pygame.draw.rect(screen,(255,255,255),pygame.Rect(x+20,200,120,120),2)
#        pygame.draw.rect(screen,(255,255,255),pygame.Rect(x+20,340,120,120),2)
#===============================================================================
        
        if self.selectMode:
            self.select.draw()
        
        if self.gameover:
            PEG_speech.Speech().setSize(30, pedo_lookup.fontList[0])
            PEG_speech.Speech().writeCentered(
                                              PEG_mainLoop.mainLoop().screen,
                                              Vector2d(250,250),
                                              "TEAM A",
                                              pedo_lookup.colorMap["A"],
                                              )
            PEG_speech.Speech().setSize(25, pedo_lookup.fontList[0])
            PEG_speech.Speech().writeCentered(
                                              PEG_mainLoop.mainLoop().screen,
                                              Vector2d(250,325),
                                              str(self.Ascore),
                                              pedo_lookup.colorMap["A"],
                                              )
            PEG_speech.Speech().setSize(30, pedo_lookup.fontList[0])
            PEG_speech.Speech().writeCentered(
                                              PEG_mainLoop.mainLoop().screen,
                                              Vector2d(525,250),
                                              "TEAM B",
                                              pedo_lookup.colorMap["B"],
                                              )
            PEG_speech.Speech().setSize(25, pedo_lookup.fontList[0])
            PEG_speech.Speech().writeCentered(
                                              PEG_mainLoop.mainLoop().screen,
                                              Vector2d(525,325),
                                              str(self.Bscore),
                                              pedo_lookup.colorMap["B"],
                                              )
            PEG_speech.Speech().setSize(45, pedo_lookup.fontList[0])
            PEG_speech.Speech().writeCentered(
                                              PEG_mainLoop.mainLoop().screen,
                                              Vector2d(387,180),
                                              "GAME OVER",
                                              FL_SELECT_YOUR_COLOR_COLOR,
                                              )
            PEG_speech.Speech().setSize(45, pedo_lookup.fontList[0])
            PEG_speech.Speech().writeCentered(
                                              PEG_mainLoop.mainLoop().screen,
                                              Vector2d(387,400),
                                              "WINNER: " + self.winner,
                                              FL_SELECT_YOUR_COLOR_COLOR,
                                              )
        
        if self.scan:
            trackPos = Vector2d(0,0)
            try:
                trackPos = PEG_mainLoop.mainLoop().entityDict['Network'].enDict[PEG_mainLoop.mainLoop().entityDict['Network'].color].pos.getPosition()
            except: print "no player identified"
            #move the camera
            PEG_mainLoop.mainLoop().cam.moveTo(trackPos)
            #get subsurface and enlarge 4x
            ss = self.fieldFake.subsurface(self.fieldFake.get_rect().clip(pygame.Rect(trackPos.x,trackPos.y,640,480)))
            #ssfourx = pygame.transform.scale2x(pygame.transform.scale2x(ss))
            ssfourx = ss
            PEG_mainLoop.mainLoop().screen.blit(ssfourx, (0,0))
#===============================================================================
#            for e in PEG_mainLoop.mainLoop().entityDict['Network'].enDict:
#                if instanceof(e,StaticEntity):
#                    #convert coordinates and draw on screen
#                    pass
#                
#===============================================================================
            #reset the camera
            PEG_mainLoop.mainLoop().cam.moveTo(Vector2d(0,0))

class StaticShapeEn(Entity):
    def __init__(self,exml = None):
        tr = Rect2d(0,0,0,0)
        self.name = ''
        self.team = "neutral"
        if exml and exml.hasAttribute("x"):
            tr.x = int(exml.getAttribute("x"))
        if exml and exml.hasAttribute("y"):
            tr.x = int(exml.getAttribute("y"))
        if exml and exml.hasAttribute("name"):
            self.name = exml.getAttribute("name")
        else: self.name = "building???"
        if exml and exml.hasAttribute("team"):
            self.team = exml.getAttribute("team")
        else: self.team = "neutral"
        if exml and exml.hasAttribute("sides"):
            self.sides = exml.getAttribute("sides")
        else: self.sides = 3
        Entity.__init__(self,tr)
        self.color = pedo_lookup.colorMap[self.team]
        
        self.visible = True
        self.state = "alive"
        self.prevState = self.state
        
        self.data = dict()
        self.data['sides'] = self.sides
        self.data['team'] = self.team
        self.data['x'] = self.pos.y
        self.data['y'] = self.pos.y
        
    def update(self):
        pass
    
    def updatePos(self):
        try:
            self.pos = Rect2d(int(self.data['x']),int(self.data['y']),0,0)
        except: pass #keep position unchanged
        
        if 'sides' in self.data: self.sides = self.data['sides']
        if not PEG_mainLoop.mainLoop().entityDict['Network'].server:
            if 'resources' in self.data: 
                self.resources = self.data['resources']
        
    def getTeam(self):
        return self.team

class BuildingEn(StaticShapeEn):
    def __init__(self,exml):
        StaticShapeEn.__init__(self,exml)
        
    def update(self):
        pass
#===============================================================================
#        if PEG_mainLoop.mainLoop().entityDict['Network'].server:
#            self.updateData()
#===============================================================================
            
    def updateData(self):
        self.data['sides'] = self.sides
        self.data['state'] = self.state
    
    def addSide(self):
        self.sides += 1
    
    def attackedBy(self,shape = None):
        if self.sides == 3:
            self.state = "destroy"
            #TODO some sort of explosion effect here maybe?
            PEG_mainLoop.mainLoop().entityDict['Network']
            network.mgr.send([data,])
            args = [3, self.pos.getPosition(), pedo_lookup.colorMap[self.team], ACTION_RADIUS]
            #PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect(PEG_effects.quickEffect(*args))
            data = PEG_effects.quickData(*args)
            network.mgr.send([data,])
        else:
            self.sides -= 1
        
    def draw(self):
        #TODO Check who I belong to
        #check who player is
        #check if close to sentry
        #draw
        flag = False
        network = PEG_mainLoop.mainLoop().entityDict['Network']
        #OR nearby a sentry
        #if network.server or pedo_lookup.teamMap[network.color] == self.team:
        if True:
            flag = True
        if flag:
            if self.sides > 1:
                pygame.draw.polygon(
                                    PEG_mainLoop.mainLoop().screen,
                                    self.color,
                                    PEG_draw.getNgon(
                                                     self.sides, 
                                                     BUILDING_DRAW_RADIUS, 
                                                     PEG_mainLoop.mainLoop().cam.convertCrds(self.pos.getPosition())),
                                    1
                                    )
            else:
                pygame.draw.circle(
                               PEG_mainLoop.mainLoop().screen,
                               self.color,
                               PEG_mainLoop.mainLoop().cam.convertCrds(self.pos.getPosition()).getIntTuple(),
                               5,
                               1
                               )
         
class PowerSourceEn(StaticShapeEn):
    def __init__(self,exml):
        StaticShapeEn.__init__(self,exml)
    def draw(self):
        #draw some pretty power source picture thingy
        if pygame.time.get_ticks() % 5 == 1:
            fx = PEG_mainLoop.mainLoop().entityDict['EffectMenu']
            fx.addEffect(PEG_effects.EffectImplodingGonNova(fx, self.pos,random.randint(300,600),25,self.color,PEG_effects.pointFunctionExpandingGonNova, random.randint(3,10)))

import PEG_effects
class ShapeEn(Entity):
    def __init__(self, exml = None):
        tr = Rect2d(0,0,0,0)
        self.name = ''
        if exml and exml.hasAttribute("x"):
            tr.x = int(exml.getAttribute("x"))
        if exml and exml.hasAttribute("y"):
            tr.x = int(exml.getAttribute("y"))
        if exml and exml.hasAttribute("name"):
            self.name = exml.getAttribute("name")
        Entity.__init__(self,tr)
        self.resources = 0
        self.sides = 3
        self.dsides = 3
        
        try: 
            self.color = pedo_lookup.colorMap[self.name]
            self.team = pedo_lookup.teamMap[self.name]
        except: 
            print "no such color found", self.name
            self.color = (255,255,255)
            self.team = "neutral"
        
        
        self.visible = True
        self.state = "alive"
        self.prevState = "alive"
        
        self.data = dict()
        self.data['sides'] = self.sides
        self.data['resources'] = self.resources
        self.data['x'] = self.pos.y
        self.data['y'] = self.pos.y
        
        self.controlled = False
        self.lastMouseUpdate = pygame.time.get_ticks()

        
        #populate this with current position
        #TODO this only needs to be done on the SERVER
        self.lastPos = []
        for i in range(LAST_POS_QUEUE_LEN):
            self.lastPos.append(self.pos.getPosition())
            
        self.playerAngleDict = dict()
        
        self.actionStart = pygame.time.get_ticks()
        self.timeSinceLastAction = pygame.time.get_ticks()
        self.currentAction = ''
        
        
    def update(self):   
        #for manual input version of game
        if self.controlled:
            #grab input
            mousePos = getVector2dFromTuple(pygame.mouse.get_pos())
            #advance current position
            newPos = self.pos.getPosition()
            if (newPos-mousePos).magnitude() > 6: 
                newPos += (mousePos - newPos).getNormal()*5 
            
            #update our own position because there is too much network lag :(
            self.pos.x = newPos.x
            self.pos.y = newPos.y
            
            data = dict()
            data['x'] = int(newPos.x)
            data['y'] = int(newPos.y)
            data['name'] = self.name
            data['type'] = 'position'
            data['entity'] = 'ShapeEn'
            network = PEG_mainLoop.mainLoop().entityDict['Network']
            network.mgr.send( [data,])
            
            
    #server calls this to tell shapeen to update its data
    def updateData(self):
        self.data['sides'] = self.sides
        self.data['resources'] = self.resources
        self.data['state'] = self.state
        
    def updatePos(self):
        if not self.controlled:
            try:
                #TODO we should get rid of this list of data. slows things down a lot ya.
                self.pos = Rect2d(int(self.data['x']),int(self.data['y']),0,0)
                self.lastPos.pop(0)
                self.lastPos.append(Vector2d(int(self.data['x']),int(self.data['y'])))
            except: pass #keep position unchanged
        
        #server dictates sides and resources
        if not PEG_mainLoop.mainLoop().entityDict['Network'].server:
            if 'sides' in self.data: self.sides = self.data['sides']
            if 'resources' in self.data: self.resources = self.data['resources']
            if 'state' in self.data: self.state = self.data['state']
    #server specific function
    
    def addResource(self):
        if self.resources < self.sides:
            self.resources += 1
        print self.name, "has", self.resources, "resources"
    #server specific fcn
    
    def attackedBy(self,shape = None):
        if self.resources > 0:
            self.resources = 0
        else:
            if self.sides > 3:
                self.sides -= 1
            elif self.state == "alive":
                self.destroySelf()
                
        
    
    def drawSelfLines(self):
        
        pygame.draw.lines(
                          PEG_mainLoop.mainLoop().screen,
                          self.color,
                          False,
                          convertToTupleList(map(PEG_mainLoop.mainLoop().cam.convertCrds,self.lastPos))                   
                         )
        
    def drawConnectingLines(self,target):
        """draws lines representing angle gain between self and target
        
        target: ShapeEn"""
        skip = 5
        for i in range(PLAYER_ANGLE_QUEUE_LEN/skip - skip):
            at = i*skip
            pygame.draw.lines(
                          PEG_mainLoop.mainLoop().screen,
                          self.color,
                          False,
                          (self.lastPos[at].getIntTuple(), 
                           target.lastPos[at].getIntTuple(),
                           target.lastPos[at+skip].getIntTuple(),
                           self.lastPos[at+skip].getIntTuple())                   
                         )
        
    def drawAnglePie(self,target):
        """draws pie chart representing angle gain as calculated by Network
        
        target: ShapeEn"""
        try: angle = -(math.fabs(reduce(add, self.playerAngleDict[target.name])) - math.fabs(reduce(add, target.playerAngleDict[self.name])))
        except: return #exception happens when player trys to graph him or herself
        
        radius = 100
        location = pygame.Rect(0,0,radius,radius)
        sfc = pygame.Surface((radius+5,radius+5),pygame.SRCALPHA)
        sfc.fill((0,0,0))
        
        pygame.draw.circle(sfc,(255,255,255),location.center,radius,1)
        pygame.draw.line(sfc,COLOR_CHARTREUSE,location.center, 
                         (getVector2dFromTuple(location.center)+getVector2dFromPolar(radius - 4,-ENCIRCLE_RADIANS+math.pi/2)).getIntTuple()
                         )
        pygame.draw.line(sfc,COLOR_RED,location.center, 
                         (getVector2dFromTuple(location.center)+getVector2dFromPolar(radius - 4,ENCIRCLE_RADIANS+math.pi/2)).getIntTuple()
                         )
        
        #TODO: maybe flash the arc as it gets closer
        if angle < 0: 
            clr = COLOR_CHARTREUSE
            pygame.draw.arc(sfc,clr,location,angle+math.pi/2,0+math.pi/2,1)
        else:
            clr = COLOR_RED
            pygame.draw.arc(sfc,clr,location,0+math.pi/2,angle+math.pi/2,1)
            
        pygame.draw.line(sfc,clr,location.center, 
                         (getVector2dFromTuple(location.center)+getVector2dFromPolar( -radius + 4,0+math.pi/2)).getIntTuple()
                         )
        pygame.draw.line(sfc,clr,location.center, 
                         (getVector2dFromTuple(location.center)+getVector2dFromPolar( -radius + 4,-angle+math.pi/2)).getIntTuple()
                         )
        #TODO blit with transparency
        PEG_mainLoop.mainLoop().screen.blit(sfc,(650,200))
        
    def draw(self):     
        #self.drawSelfLines()
#===============================================================================
#        pygame.draw.circle(
#                           PEG_mainLoop.mainLoop().screen,
#                           FL_COLOR_OUTER_RADIUS_CIRCLE,
#                           PEG_mainLoop.mainLoop().cam.convertCrds(self.pos.getPosition()).getIntTuple(),
#                           OUTER_RADIUS/2,
#                           1
#                           )
#===============================================================================
        
        #TODO we should make it so we only draw resources if one is on the same team
        if self.resources == 1 or self.sides < 3:
            pygame.draw.circle(
                   PEG_mainLoop.mainLoop().screen,
                   self.color,
                   PEG_mainLoop.mainLoop().cam.convertCrds(self.pos.getPosition()).getIntTuple(),
                   3,
                   1
                   )
            
        #TODO this is areally bad fix, come up with a better one.
        try:
            if self.resources > 1:
                print self.resources
                pygame.draw.polygon(
                                    PEG_mainLoop.mainLoop().screen,
                                    (255,255,255),
                                    PEG_draw.getNgon(
                                                     self.resources, 
                                                     PLAYER_RESOURCE_DRAW_RADIUS, 
                                                     PEG_mainLoop.mainLoop().cam.convertCrds(self.pos.getPosition())),
                                    1
                                    )
        except: print "drawing at line 658 in PEG_entity screwed up as usual :(((("
            
#===============================================================================
#        pygame.draw.circle(
#                           PEG_mainLoop.mainLoop().screen,
#                           FL_COLOR_INNER_RADIUS_CIRCLE,
#                           PEG_mainLoop.mainLoop().cam.convertCrds(self.pos.getPosition()).getIntTuple(),
#                           INNER_RADIUS,
#                           1
#                           )
#===============================================================================
        
        if self.sides == 999:
            if pygame.time.get_ticks() % 3 == 1:
                self.dsides = random.randint(3,10)
        else:
            self.dsides = self.sides
            
        #TODO do exception case when sides == 0 we should draw something else (like a circle)
        #TODO FIX SOME BUG HERE????
        if self.dsides > 1:
            pygame.draw.polygon(
                                PEG_mainLoop.mainLoop().screen,
                                self.color,
                                PEG_draw.getNgon(
                                                 self.dsides, 
                                                 PLAYER_DRAW_RADIUS, 
                                                 PEG_mainLoop.mainLoop().cam.convertCrds(self.pos.getPosition())),
                                1
                                )
        
        if self.controlled:
            self.serialRoutine()
                
    #network specific
    def destroySelf(self):
        self.state = "dead"
        self.sides = 0
        self.resources = 0
        args = [3, self.pos.getPosition(), COLOR_CHARTREUSE, 150]
        #PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect(PEG_effects.quickEffect(*args))
        network = PEG_mainLoop.mainLoop().entityDict['Network']
        network.mgr.send([PEG_effects.quickData(*args),])
            
    def serialRoutineOLD(self):
        serial = PEG_mainLoop.mainLoop().entityDict["SerialEntity"]
        action = serial.getActionString()
        if action == "starting sweep":
            print "sweep started"
            self.currentAction = action
            self.actionStart = pygame.time.get_ticks()
        elif action == "triggered sweep":
            print "sweep triggered"
            self.currentAction = ''
            network = PEG_mainLoop.mainLoop().entityDict['Network']
            data = dict()
            data['type'] = 'event'
            data['event'] = 'arduino'
            data['name'] = self.color
            data['arduino'] = action
            network.mgr.send([data,])
            args = [5, self.pos.getPosition(), self.color, ACTION_RADIUS]
            #PEG_mainLoop.mainLoop().entityDict['EffectMenu'].addEffect(PEG_effects.quickEffect(*args))
            data = PEG_effects.quickData(*args)
            data['relay'] = 'True'
            network.mgr.send([data,])
            
            
        if self.currentAction == "starting sweep":
            timeSinceLastAction = pygame.time.get_ticks() - self.actionStart
            pygame.draw.circle(
                               PEG_mainLoop.mainLoop().screen,
                               COLOR_GRAY,
                               PEG_mainLoop.mainLoop().cam.convertCrds(self.pos.getPosition()).getIntTuple(),
                               ACTION_RADIUS*timeSinceLastAction/ACTION_TIME + 1,
                               1
                               )
            pygame.draw.circle(
                               PEG_mainLoop.mainLoop().screen,
                               self.color,
                               PEG_mainLoop.mainLoop().cam.convertCrds(self.pos.getPosition()).getIntTuple(),
                               ACTION_RADIUS,
                               1
                               )
    def serialRoutine(self):
        serial = PEG_mainLoop.mainLoop().entityDict["SerialEntity"]
        action = serial.getAction()
        if( action != ''):
            if self.currentAction != action:
                self.actionStart = pygame.time.get_ticks()
                self.currentAction = action
                
            self.timeSinceLastAction = pygame.time.get_ticks() - self.actionStart
            if self.timeSinceLastAction < ACTION_TIME:
                pygame.draw.circle(
                                   PEG_mainLoop.mainLoop().screen,
                                   COLOR_GRAY,
                                   PEG_mainLoop.mainLoop().cam.convertCrds(self.pos.getPosition()).getIntTuple(),
                                   ACTION_RADIUS*float(self.timeSinceLastAction)/float(ACTION_TIME) + 1,
                                   1
                                   )
                pygame.draw.circle(
                                   PEG_mainLoop.mainLoop().screen,
                                   self.color,
                                   PEG_mainLoop.mainLoop().cam.convertCrds(self.pos.getPosition()).getIntTuple(),
                                   ACTION_RADIUS,
                                   1
                                   )
            else:
                self.currentAction = ''
                #TODO
                #send net message
                network = PEG_mainLoop.mainLoop().entityDict['Network']
                data = dict()
                data['type'] = 'event'
                data['event'] = 'arduino'
                data['name'] = self.name
                data['arduino'] = action
                network.mgr.send([data,])
        else:
            #or for the type of action where radius is determined by time, check for this and send a message
            self.currentAction = ''
            
    def getTeam(self):
        return pedo_lookup.teamMap[self.name]
    

 