import pygame
from PEG_datatypes import *
from PEG_constants import *
import PEG_helpers
import PEG_mainLoop
import PEG_speech 
import pedo_lookup

class TextButton01:
    def __init__(self, rect = Rect2d(0,0,0,0), color = (0,0,0), text = ""):
        self.pos = rect
        self.borderColor = color
        self.color = color
        self.text = text
        pass
    
    def drawCam(self):
        print "not implemented"
        
    def drawAbs(self):
        pygame.draw.rect(PEG_mainLoop.mainLoop().screen,self.borderColor,self.pos.getSDLRect(), 2)
        PEG_speech.Speech().setSize(30, pedo_lookup.fontList[0])
        PEG_speech.Speech().writeCentered(PEG_mainLoop.mainLoop().screen, self.pos.getCenter(),self.text, self.color)
        
        pass
    
    def isPointInside(self,pos):
        return PEG_helpers.livesIn(pos,self.pos)
    
    def isRectInside(self,rect):
        print "not implemented"